@RELATION "horses"
@ATTRIBUTE  surgery {1,2}
%          1 = Yes, it had surgery
%          2 = It was treated without surgery

@ATTRIBUTE age  {1,2}
%          1 = Adult horse
%          2 = Young (< 6 months)

@ATTRIBUTE hospital_number numeric
%          - numeric id
%          - the case number assigned to the horse
%            (may not be unique if the horse is treated > 1 time)

@ATTRIBUTE rectal_temperature numeric
%          - linear
%          - in degrees celsius.
%          - An elevated temp may occur due to infection.
%          - temperature may be reduced when the animal is in late shock
%          - normal temp is 37.8
%          - this parameter will usually change as the problem progresses
%               eg. may start out normal, then become elevated because of
%                   the lesion, passing back through the normal range as the
%                   horse goes into shock
@ATTRIBUTE  pulse numeric
%          - linear
%          - the heart rate in beats per minute
%          - is a reflection of the heart condition: 30 -40 is normal for adults
%         - rare to have a lower than normal rate although athletic horses
%            may have a rate of 20-25
%          - animals with painful lesions or suffering from circulatory shock
%            may have an elevated heart rate

@ATTRIBUTE respiratory_rate numeric
%          - linear
%          - normal rate is 8 to 10
%          - usefulness is doubtful due to the great fluctuations
%6
@ATTRIBUTE temperature_of_extremities {1,2,3,4}
%          - a subjective indication of peripheral circulation
%          - possible values:
%               1 = Normal
%               2 = Warm
%               3 = Cool
%               4 = Cold
%          - cool to cold extremities indicate possible shock
%          - hot extremities should correlate with an elevated rectal temp.

@ATTRIBUTE  peripheral_pulse {1,2,3,4}
%          - subjective
%          - possible values are:
%               1 = normal
%               2 = increased
%               3 = reduced
%               4 = absent
%          - normal or increased p.p. are indicative of adequate circulation
%            while reduced or absent indicate poor perfusion

@ATTRIBUTE  mucous_membranes {1,2,3,4,5,6}
%          - a subjective measurement of colour
%          - possible values are:
%               1 = normal pink
%               2 = bright pink
%               3 = pale pink
%               4 = pale cyanotic
%               5 = bright red / injected
%               6 = dark cyanotic
%          - 1 and 2 probably indicate a normal or slightly increased
%            circulation
%          - 3 may occur in early shock
%          - 4 and 6 are indicative of serious circulatory compromise
%          - 5 is more indicative of a septicemia

@ATTRIBUTE capillary_refill_time {1,2}
%          - a clinical judgement. The longer the refill, the poorer the
%            circulation
%          - possible values
%               1 = < 3 seconds
%               2 = >= 3 seconds
%10
@ATTRIBUTE pain {1,2,3,4,5}
%          - possible values:
%               1 = alert, no pain
%               2 = depressed
%               3 = intermittent mild pain
%               4 = intermittent severe pain
%               5 = continuous severe pain
%          - should NOT be treated as a ordered or discrete variable!
%          - In general, the more painful, the more likely it is to require
%            surgery
%          - prior treatment of pain may mask the pain level to some extent

@ATTRIBUTE peristalsis  {1,2,3,4}                          
%          - an indication of the activity in the horse's gut. As the gut
%            becomes more distended or the horse becomes more toxic, the
%            activity decreases
%          - possible values:
%               1 = hypermotile
%               2 = normal
%               3 = hypomotile
%               4 = absent

@ATTRIBUTE abdominal_distension {1,2,3,4}
%          - An IMPORTANT parameter.
%          - possible values
%               1 = none
%               2 = slight
%               3 = moderate
%               4 = severe
%          - an animal with abdominal distension is likely to be painful and
%            have reduced gut motility.
%          - a horse with severe abdominal distension is likely to require
%            surgery just tio relieve the pressure

@ATTRIBUTE nasogastric_tube   {1,2,3}
%          - this refers to any gas coming out of the tube
%          - possible values:
%               1 = none
%               2 = slight
%               3 = significant
%          - a large gas cap in the stomach is likely to give the horse
%            discomfort

@ATTRIBUTE nasogastric_reflux  {1,2,3}
%          - possible values
%               1 = none
%               2 = > 1 liter
%               3 = < 1 liter
%          - the greater amount of reflux, the more likelihood that there is
%            some serious obstruction to the fluid passage from the rest of
%            the intestine
%15
@ATTRIBUTE nasogastric_reflux_PH numeric
%          - linear
%          - scale is from 0 to 14 with 7 being neutral
%          - normal values are in the 3 to 4 range

@ATTRIBUTE rectal_examination_feces numeric
%          - possible values
%               1 = normal
%               2 = increased
%               3 = decreased
%               4 = absent
%          - absent feces probably indicates an obstruction

@ATTRIBUTE abdomen {1,2,3,4,5}
%          - possible values
%               1 = normal
%               2 = other
%               3 = firm feces in the large intestine
%               4 = distended small intestine
%               5 = distended large intestine
%          - 3 is probably an obstruction caused by a mechanical impaction
%            and is normally treated medically
%          - 4 and 5 indicate a surgical lesion

@ATTRIBUTE packed_cell_volume numeric
%          - linear
%          - the # of red cells by volume in the blood
%          - normal range is 30 to 50. The level rises as the circulation
%            becomes compromised or as the animal becomes dehydrated.

@ATTRIBUTE total_protein numeric
%          - linear
%          - normal values lie in the 6-7.5 (gms/dL) range
%          - the higher the value the greater the dehydration
%20

@ATTRIBUTE abdominocentesis_appearance {1,2,3}
%          - a needle is put in the horse's abdomen and fluid is obtained from
%            the abdominal cavity
%          - possible values:
%               1 = clear
%               2 = cloudy
%               3 = serosanguinous
%          - normal fluid is clear while cloudy or serosanguinous indicates
%            a compromised gut

@ATTRIBUTE abdomcentesis_total_protein numeric
%          - linear
%          - the higher the level of protein the more likely it is to have a
%            compromised gut. Values are in gms/dL

@ATTRIBUTE outcome {1,2,3}
%          - what eventually happened to the horse?
%          - possible values:
%               1 = lived
%               2 = died
%               3 = was euthanized

@ATTRIBUTE surgical_lesion {1,2}
%          - retrospectively, was the problem (lesion) surgical?
%          - all cases are either operated upon or autopsied so that
%            this value and the lesion type are always known
%          - possible values:
%               1 = Yes
%               2 = No

@attribute class {1,2}
% 25: cp_data
%          - is pathology data present for this case?
%               1 = Yes
%               2 = No
%          - this variable is of no significance since pathology data
%            is not included or collected for these cases
%25
@DATA
2,1,530101,38.50,66,28,3,3,?,2,5,4,4,?,?,?,3,5,45.00,8.40,?,?,2,2,2
1,1,534817,39.2,88,20,?,?,4,1,3,4,2,?,?,?,4,2,50,85,2,2,3,2,2
2,1,530334,38.30,40,24,1,1,3,1,3,3,1,?,?,?,1,1,33.00,6.70,?,?,1,2,1
1,2,5290409,39.10,164,84,4,1,6,2,2,4,4,1,2,5.00,3,?,48.00,7.20,3,5.30,2,1,1
2,1,530255,37.30,104,35,?,?,6,2,?,?,?,?,?,?,?,?,74.00,7.40,?,?,2,2,2
2,1,528355,?,?,?,2,1,3,1,2,3,2,2,1,?,3,3,?,?,?,?,1,2,2
1,1,526802,37.90,48,16,1,1,1,1,3,3,3,1,1,?,3,5,37.00,7.00,?,?,1,1,2
1,1,529607,?,60,?,3,?,?,1,?,4,2,2,1,?,3,4,44.00,8.30,?,?,2,1,2
2,1,530051,?,80,36,3,4,3,1,4,4,4,2,1,?,3,5,38.00,6.20,?,?,3,1,2
2,2,5299629,38.30,90,?,1,?,1,1,5,3,1,2,1,?,3,?,40.00,6.20,1,2.20,1,2,1
1,1,528548,38.10,66,12,3,3,5,1,3,3,1,2,1,3.00,2,5,44.00,6.00,2,3.60,1,1,1
2,1,527927,39.10,72,52,2,?,2,1,2,1,2,1,1,?,4,4,50.00,7.80,?,?,1,1,2
1,1,528031,37.20,42,12,2,1,1,1,3,3,3,3,1,?,4,5,?,7.00,?,?,1,2,2
2,2,5291329,38.00,92,28,1,1,2,1,1,3,2,3,?,7.20,1,1,37.00,6.10,1,?,2,2,1
1,1,534917,38.2,76,28,3,1,1,1,3,4,1,2,2,?,4,4,46,81,1,2,1,1,2
1,1,530233,37.60,96,48,3,1,4,1,5,3,3,2,3,4.50,4,?,45.00,6.80,?,?,2,1,2
1,2,5301219,?,128,36,3,3,4,2,4,4,3,3,?,?,4,5,53.00,7.80,3,4.70,2,2,1
2,1,526639,37.50,48,24,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,2,2
1,1,5290481,37.60,64,21,1,1,2,1,2,3,1,1,1,?,2,5,40.00,7.00,1,?,1,1,1
2,1,532110,39.4,110,35,4,3,6,?,?,3,3,?,?,?,?,?,55,8.7,?,?,1,2,2
1,1,530157,39.90,72,60,1,1,5,2,5,4,4,3,1,?,4,4,46.00,6.10,2,?,1,1,2
2,1,529340,38.40,48,16,1,?,1,1,1,3,1,2,3,5.50,4,3,49.00,6.80,?,?,1,2,2
1,1,521681,38.60,42,34,2,1,4,?,2,3,1,?,?,?,1,?,48.00,7.20,?,?,1,1,2
1,2,534998,38.3,130,60,?,3,?,1,2,4,?,?,?,?,?,?,50,70,?,?,1,1,2
1,1,533692,38.1,60,12,3,3,3,1,?,4,3,3,2,2,?,?,51,65,?,?,1,1,2
2,1,529518,37.80,60,42,?,?,?,1,?,?,?,?,?,?,?,?,?,?,?,?,1,2,2
1,1,530526,38.30,72,30,4,3,3,2,3,3,3,2,1,?,3,5,43.00,7.00,2,3.90,1,1,1
1,1,528653,37.80,48,12,3,1,1,1,?,3,2,1,1,?,1,3,37.00,5.50,2,1.30,1,2,1
1,1,5279442,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,2,2,2
2,1,535415,37.7,48,?,2,1,1,1,1,1,1,1,1,?,?,?,45,76,?,?,1,2,2
2,1,529475,37.70,96,30,3,3,4,2,5,4,4,3,2,4.00,4,5,66.00,7.50,?,?,2,1,2
2,1,530242,37.20,108,12,3,3,4,2,2,4,2,?,3,6.00,3,3,52.00,8.20,3,7.40,3,1,1
1,1,529427,37.20,60,?,2,1,1,1,3,3,3,2,1,?,4,5,43.00,6.60,?,?,1,1,2
1,1,529663,38.20,64,28,1,1,1,1,3,1,?,?,?,?,4,4,49.00,8.60,2,6.60,1,1,1
1,1,529796,?,100,30,3,3,4,2,5,4,4,3,3,?,4,4,52.00,6.60,?,?,1,1,2
2,1,528812,?,104,24,4,3,3,2,4,4,3,?,3,?,?,2,73.00,8.40,?,?,3,1,2
2,1,529493,38.30,112,16,?,3,5,2,?,?,1,1,2,?,?,5,51.00,6.00,2,1.00,3,2,1
1,1,533847,37.8,72,?,?,3,?,1,5,3,1,?,1,?,1,1,56,80,1,2,1,1,2
2,1,528996,38.60,52,?,1,1,1,1,3,3,2,1,1,?,1,3,32.00,6.60,1,5.00,1,2,1
1,2,5277409,39.20,146,96,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,2,1,2
1,1,529498,?,88,?,3,3,6,2,5,3,3,1,3,?,4,5,63.00,6.50,3,?,2,1,2
2,2,5288249,39.00,150,72,?,?,?,?,?,?,?,?,?,?,?,?,47.00,8.50,?,0.10,1,1,1
2,1,530301,38.00,60,12,3,1,3,1,3,3,1,1,1,?,2,2,47.00,7.00,?,?,1,2,2
1,1,534069,?,120,?,3,4,4,1,4,4,4,1,1,?,?,5,52,67,2,2,3,1,2
1,1,535407,35.4,140,24,3,3,4,2,4,4,?,2,1,?,?,5,57,69,3,2,3,1,2
2,1,529827,?,120,?,4,3,4,2,5,4,4,1,1,?,4,5,60.00,6.50,3,?,2,1,2
1,1,529888,37.90,60,15,3,?,4,2,5,4,4,2,2,?,4,5,65.00,7.50,?,?,1,1,1
2,1,529821,37.50,48,16,1,1,1,1,1,1,1,1,1,?,1,?,37.00,6.50,?,?,1,2,2
1,1,528890,38.90,80,44,3,3,3,2,2,3,3,2,2,7.00,3,1,54.00,6.50,3,?,2,1,2
2,1,529642,37.20,84,48,3,3,5,2,4,1,2,1,2,?,2,1,73.00,5.50,2,4.10,2,2,1
2,1,529766,38.60,46,?,1,1,2,1,1,3,2,1,1,?,?,2,49.00,9.10,1,1.60,1,2,1
1,1,527706,37.40,84,36,1,?,3,2,3,3,2,?,?,?,4,5,?,?,3,?,2,1,1
2,1,529483,?,?,?,1,1,3,1,1,3,1,?,?,?,2,2,43.00,7.70,?,?,1,2,2
2,1,530544,38.60,40,20,?,?,?,1,?,?,?,?,?,?,?,?,41.00,6.40,?,?,1,2,1
2,1,529461,40.30,114,36,3,3,1,2,2,3,3,2,1,7.00,1,5,57.00,8.10,3,4.50,3,1,1
