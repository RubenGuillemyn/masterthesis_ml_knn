@RELATION "horsesTest"
@ATTRIBUTE  surgery {1,2}
%          1 = Yes, it had surgery
%          2 = It was treated without surgery

@ATTRIBUTE age  {1,2}
%          1 = Adult horse
%          2 = Young (< 6 months)

@ATTRIBUTE hospital_number numeric
%          - numeric id
%          - the case number assigned to the horse
%            (may not be unique if the horse is treated > 1 time)

@ATTRIBUTE rectal_temperature numeric
%          - linear
%          - in degrees celsius.
%          - An elevated temp may occur due to infection.
%          - temperature may be reduced when the animal is in late shock
%          - normal temp is 37.8
%          - this parameter will usually change as the problem progresses
%               eg. may start out normal, then become elevated because of
%                   the lesion, passing back through the normal range as the
%                   horse goes into shock
@ATTRIBUTE  pulse numeric
%          - linear
%          - the heart rate in beats per minute
%          - is a reflection of the heart condition: 30 -40 is normal for adults
%         - rare to have a lower than normal rate although athletic horses
%            may have a rate of 20-25
%          - animals with painful lesions or suffering from circulatory shock
%            may have an elevated heart rate

@ATTRIBUTE respiratory_rate numeric
%          - linear
%          - normal rate is 8 to 10
%          - usefulness is doubtful due to the great fluctuations
%6
@ATTRIBUTE temperature_of_extremities {1,2,3,4}
%          - a subjective indication of peripheral circulation
%          - possible values:
%               1 = Normal
%               2 = Warm
%               3 = Cool
%               4 = Cold
%          - cool to cold extremities indicate possible shock
%          - hot extremities should correlate with an elevated rectal temp.

@ATTRIBUTE  peripheral_pulse {1,2,3,4}
%          - subjective
%          - possible values are:
%               1 = normal
%               2 = increased
%               3 = reduced
%               4 = absent
%          - normal or increased p.p. are indicative of adequate circulation
%            while reduced or absent indicate poor perfusion

@ATTRIBUTE  mucous_membranes {1,2,3,4,5,6}
%          - a subjective measurement of colour
%          - possible values are:
%               1 = normal pink
%               2 = bright pink
%               3 = pale pink
%               4 = pale cyanotic
%               5 = bright red / injected
%               6 = dark cyanotic
%          - 1 and 2 probably indicate a normal or slightly increased
%            circulation
%          - 3 may occur in early shock
%          - 4 and 6 are indicative of serious circulatory compromise
%          - 5 is more indicative of a septicemia

@ATTRIBUTE capillary_refill_time {1,2}
%          - a clinical judgement. The longer the refill, the poorer the
%            circulation
%          - possible values
%               1 = < 3 seconds
%               2 = >= 3 seconds
%10
@ATTRIBUTE pain {1,2,3,4,5}
%          - possible values:
%               1 = alert, no pain
%               2 = depressed
%               3 = intermittent mild pain
%               4 = intermittent severe pain
%               5 = continuous severe pain
%          - should NOT be treated as a ordered or discrete variable!
%          - In general, the more painful, the more likely it is to require
%            surgery
%          - prior treatment of pain may mask the pain level to some extent

@ATTRIBUTE peristalsis  {1,2,3,4}                          
%          - an indication of the activity in the horse's gut. As the gut
%            becomes more distended or the horse becomes more toxic, the
%            activity decreases
%          - possible values:
%               1 = hypermotile
%               2 = normal
%               3 = hypomotile
%               4 = absent

@ATTRIBUTE abdominal_distension {1,2,3,4}
%          - An IMPORTANT parameter.
%          - possible values
%               1 = none
%               2 = slight
%               3 = moderate
%               4 = severe
%          - an animal with abdominal distension is likely to be painful and
%            have reduced gut motility.
%          - a horse with severe abdominal distension is likely to require
%            surgery just tio relieve the pressure

@ATTRIBUTE nasogastric_tube   {1,2,3}
%          - this refers to any gas coming out of the tube
%          - possible values:
%               1 = none
%               2 = slight
%               3 = significant
%          - a large gas cap in the stomach is likely to give the horse
%            discomfort

@ATTRIBUTE nasogastric_reflux  {1,2,3}
%          - possible values
%               1 = none
%               2 = > 1 liter
%               3 = < 1 liter
%          - the greater amount of reflux, the more likelihood that there is
%            some serious obstruction to the fluid passage from the rest of
%            the intestine
%15
@ATTRIBUTE nasogastric_reflux_PH numeric
%          - linear
%          - scale is from 0 to 14 with 7 being neutral
%          - normal values are in the 3 to 4 range

@ATTRIBUTE rectal_examination_feces numeric
%          - possible values
%               1 = normal
%               2 = increased
%               3 = decreased
%               4 = absent
%          - absent feces probably indicates an obstruction

@ATTRIBUTE abdomen {1,2,3,4,5}
%          - possible values
%               1 = normal
%               2 = other
%               3 = firm feces in the large intestine
%               4 = distended small intestine
%               5 = distended large intestine
%          - 3 is probably an obstruction caused by a mechanical impaction
%            and is normally treated medically
%          - 4 and 5 indicate a surgical lesion

@ATTRIBUTE packed_cell_volume numeric
%          - linear
%          - the # of red cells by volume in the blood
%          - normal range is 30 to 50. The level rises as the circulation
%            becomes compromised or as the animal becomes dehydrated.

@ATTRIBUTE total_protein numeric
%          - linear
%          - normal values lie in the 6-7.5 (gms/dL) range
%          - the higher the value the greater the dehydration
%20

@ATTRIBUTE abdominocentesis_appearance {1,2,3}
%          - a needle is put in the horse's abdomen and fluid is obtained from
%            the abdominal cavity
%          - possible values:
%               1 = clear
%               2 = cloudy
%               3 = serosanguinous
%          - normal fluid is clear while cloudy or serosanguinous indicates
%            a compromised gut

@ATTRIBUTE abdomcentesis_total_protein numeric
%          - linear
%          - the higher the level of protein the more likely it is to have a
%            compromised gut. Values are in gms/dL

@ATTRIBUTE outcome {1,2,3}
%          - what eventually happened to the horse?
%          - possible values:
%               1 = lived
%               2 = died
%               3 = was euthanized

@ATTRIBUTE surgical_lesion {1,2}
%          - retrospectively, was the problem (lesion) surgical?
%          - all cases are either operated upon or autopsied so that
%            this value and the lesion type are always known
%          - possible values:
%               1 = Yes
%               2 = No

@attribute class {1,2}
% 25: cp_data
%          - is pathology data present for this case?
%               1 = Yes
%               2 = No
%          - this variable is of no significance since pathology data
%            is not included or collected for these cases
%25
@DATA
1,1,527883,39.30,64,90,2,3,1,1,?,3,1,1,2,6.50,1,5,39.00,6.70,?,?,1,1,2
1,1,528570,37.50,60,50,3,3,1,1,3,3,2,2,2,3.50,3,4,35.00,6.50,?,?,2,1,2
1,1,534626,37.7,80,?,3,3,6,1,5,4,1,2,3,?,3,1,50,55,3,2,1,1,2
1,1,529796,?,100,30,3,3,4,2,5,4,4,3,3,?,4,4,52.00,6.60,?,?,1,1,2
1,1,528638,37.70,120,28,3,3,3,1,5,3,3,1,1,?,?,?,65.00,7.00,3,?,2,1,1
1,1,534624,?,76,?,?,3,?,?,?,4,4,?,?,?,?,5,?,?,?,?,3,1,2
1,2,5297159,38.80,150,50,1,3,6,2,5,3,2,1,1,?,?,?,50.00,6.20,?,?,2,1,2
1,1,534787,38.0,36,16,3,1,1,1,4,2,2,3,3,2,3,?,37,75,2,1,3,2,2
2,1,528620,36.90,50,40,2,3,3,1,1,3,2,3,1,7.00,?,?,37.50,6.50,?,?,1,2,2
2,1,528019,37.80,40,16,1,1,1,1,1,1,1,?,?,?,1,1,37.00,6.80,?,?,1,2,2
2,1,529172,38.20,56,40,4,3,1,1,2,4,3,2,2,7.50,?,?,47.00,7.20,1,2.50,1,2,1
1,1,534644,38.6,48,12,?,?,1,?,1,1,?,?,?,?,?,?,36,67,?,?,1,2,2
2,1,530624,40.00,78,?,3,3,5,1,2,3,1,1,1,?,4,1,66.00,6.50,?,?,2,1,1
1,1,527544,?,70,16,3,4,5,2,2,3,2,2,1,?,4,5,60.00,7.50,?,?,2,1,2
1,1,527758,38.20,72,18,?,?,?,?,?,?,?,?,?,?,?,?,35.00,6.40,?,?,1,1,2
2,1,530439,38.50,54,?,1,1,1,1,3,1,1,2,1,?,1,?,40.00,6.80,2,7.00,1,2,1
1,1,5283431,38.50,66,24,1,1,1,1,3,3,1,2,1,?,4,5,40.00,6.70,1,?,1,1,1
2,1,5275212,37.80,82,12,3,1,1,2,4,?,3,1,3,?,?,?,50.00,7.00,?,?,3,1,2
2,2,5305129,39.50,84,30,?,?,?,1,?,?,?,?,?,?,?,?,28.00,5.00,?,?,1,2,2
1,1,529428,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,1,2
1,1,529126,38.00,50,36,?,1,1,1,3,2,2,?,?,?,3,?,39.00,6.60,1,5.30,1,1,1
2,1,535054,38.6,45,16,2,1,2,1,1,1,?,?,?,?,1,1,43,58,?,?,1,2,2
1,1,528890,38.90,80,44,3,3,3,1,2,3,3,2,2,7.00,3,1,54.00,6.50,3,?,2,1,2
1,1,530034,37.00,66,20,1,3,2,1,4,3,3,1,?,?,1,5,35.00,6.90,2,?,2,1,2
1,1,534004,?,78,24,3,3,3,1,?,3,?,2,1,?,?,4,43,62,?,2,3,2,2
2,1,533902,38.5,40,16,1,1,1,1,2,1,1,?,?,?,3,2,37,67,?,?,1,2,2
1,1,533886,?,120,70,4,?,4,2,2,4,?,?,?,?,?,5,55,65,?,?,3,2,2
2,1,527702,37.20,72,24,3,2,4,2,4,3,3,3,1,?,4,4,44.00,?,3,3.30,3,1,1
1,1,529386,37.50,72,30,4,3,4,1,4,4,3,2,1,?,3,5,60.00,6.80,?,?,2,1,2
1,1,530612,36.50,100,24,3,3,3,1,3,3,3,3,1,?,4,4,50.00,6.00,3,3.40,1,1,1
1,1,534618,37.2,40,20,?,?,?,?,?,?,?,?,?,?,4,1,36,62,1,1,3,2,2
