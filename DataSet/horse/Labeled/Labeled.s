[Data]
File = /home/ruben/DataSet/horse/Labeled/horseColicLabeled.arff
TestSet = /home/ruben/DataSet/horse/Test/horseColicTest.arff
[Attributes]
Target = 24
% Als geen target is ingesteld, dan wordt standaard de laatste genomen
% als target is ingevuld is op 1 waarde, dan neemt hij rest als descriptive
[Ensemble]
EnsembleMethod = RForest
Iterations = 50
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 6
