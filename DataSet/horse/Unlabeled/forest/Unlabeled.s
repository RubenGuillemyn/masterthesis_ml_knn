[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
SelectRandomSubspaces = 5
PrintAllModels=Yes

[Attributes]
Descriptive=1-23,25
Disable=24
[Data]
File = Unlabeled.arff
TestSet = test.arff
