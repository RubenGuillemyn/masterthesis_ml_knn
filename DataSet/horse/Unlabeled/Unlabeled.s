[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 6

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = Yes
Optimize = Yes
PrintPaths = Yes
SelectRandomSubspaces = 5

[Data]
File = ../Unlabeled.arff
TestSet = .arff
