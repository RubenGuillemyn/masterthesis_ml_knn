[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 5

[Attributes]
Descriptive=1-21
Disable=22
Target=1-21
Clustering=1-21
[Data]
File = lu.arff
TestSet = test.arff
