[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 2

[Attributes]
Descriptive=1-4
Disable=5
Target=1-4
Clustering=1-4
[Data]
File = lu.arff
TestSet = test.arff
