[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 7

[Attributes]
Descriptive=1-48
Disable=49
Target=1-48
Clustering=1-48
[Data]
File = lu.arff
TestSet = test.arff
