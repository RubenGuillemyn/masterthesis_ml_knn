[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 6

[Attributes]
Descriptive=1-41
Disable=42
Target=1-41
Clustering=1-41
[Data]
File = lu.arff
TestSet = test.arff
