[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = Infinity

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 13

[Attributes]
Descriptive=1-166
Disable=167
Target=1-166
Clustering=1-166
[Data]
File = lu.arff
TestSet = test.arff
