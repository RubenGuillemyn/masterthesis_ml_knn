[General]
RandomSeed = 1
Verbose = 1

[Data]
File = labeled.arff
TestSet = test.arff

[Attributes]
Target = 5
Descriptive = 1-4

[Tree]
Heuristic = VarianceReduction

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
PrintPaths = Yes

[SemiSupervised]
UnlabeledData = lu.arff
SemiSupervisedMethod = PCT

[Output]
TrainErrors = Yes
TestErrors = Yes
