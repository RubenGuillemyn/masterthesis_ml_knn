[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 5

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 10

[Attributes]
Descriptive=1-104
Disable=105
Target=1-104
Clustering=1-104
[Data]
File = lu.arff
TestSet = test.arff
