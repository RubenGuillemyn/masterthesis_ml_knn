Clus v2.12 - Software for Predictive Clustering

Copyright (C) 2007, 2008, 2009, 2010
   Katholieke Universiteit Leuven, Leuven, Belgium
   Jozef Stefan Institute, Ljubljana, Slovenia

This program is free software and comes with ABSOLUTELY NO
WARRANTY. You are welcome to redistribute it under certain
conditions. Type 'clus -copying' for distribution details.

Loading 'labeled'

Reading ARFF Header

Reading CSV Data
Found 47 rows
Space required by nominal attributes: 4 bytes/tuple regular, 0 bytes/tuple bitwise
Clustering: Weights C=[1], N=[2.227]

Has missing values: false
Run: 01
Ensemble Classifier

Memory And Time Optimization = false
Out-Of-Bag Estimate of the error = false
	Perform Feature Ranking = false
Ensemble Method: Random Forest
Loading 'test.arff'
Bag: 1
Clustering: Weights C=[1], N=[1]
Test: surgery=2 > 0.0 -> 0.28696161870118775
Test: outcome=3 > 0.0 -> 0.2092656877781447
Test: temperature_of_extremities=3 > 0.446281 -> 0.5487949406953987
Test: nasogastric_tube=3 > 0.113402 -> 0.4204484631347319
Test: abdomen=5 > 0.0 -> 0.26899559358928116
Test: abdominal_distension=2 > 0.0 -> 1.0
Bag: 2
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.314815 -> 0.16245554429816167
Test: nasogastric_reflux=3 > 0.0 -> 0.13523033759999703
Test: rectal_temperature > 0.513461 -> 0.17545941726494152
Test: packed_cell_volume > 0.307692 -> 0.4464393446710156
Test: abdominocentesis_appearance=2 > 0.0 -> 0.2935644431995964
Test: temperature_of_extremities=4 > 0.0 -> 1.0
Test: surgery=2 > 0.0 -> 0.18344689162264094
Test: rectal_temperature > 0.513461 -> 0.6500224216483541
Test: mucous_membranes=6 > 0.0 -> 0.5297257989969674
Test: total_protein > 0.056009 -> 0.46899559358928117
Bag: 3
Clustering: Weights C=[1], N=[1]
Test: pulse > 0.123377 -> 0.1420506069806622
Test: nasogastric_tube=3 > 0.0 -> 0.15136285442184472
Test: mucous_membranes=3 > 0.0 -> 0.20614639579115884
Test: abdominocentesis_appearance=1 > 0.0 -> 0.9182958340544894
Test: peristalsis=2 > 0.0 -> 0.3912435636292556
Test: abdominal_distension=1 > 0.0 -> 0.1215782307355292
Test: respiratory_rate > 0.147727 -> 0.5487949406953987
Test: surgery=2 > 0.0 -> 0.8112781244591328
Test: nasogastric_reflux=1 > 0.625 -> 0.2569811892423542
Test: mucous_membranes=3 > 0.0 -> 0.9852281360342515
Bag: 4
Clustering: Weights C=[1], N=[1]
Test: mucous_membranes=6 > 0.079681 -> 0.11482614596892393
Test: total_protein > 0.056009 -> 0.45380310285662384
Test: surgery=2 > 0.0 -> 0.33073028860391074
Test: pain=2 > 0.0 -> 0.6500224216483541
Test: outcome=1 > 0.0 -> 0.19845496251770922
Test: abdomen=5 > 0.0 -> 0.9182958340544894
Bag: 5
Clustering: Weights C=[1], N=[1]
Test: outcome=2 > 0.0 -> 0.3679981909020412
Test: abdomcentesis_total_protein > 0.29396 -> 0.2169698806152418
Test: temperature_of_extremities=1 > 0.0 -> 0.15414882405609115
Test: pain=3 > 0.271605 -> 0.303307408607834
Test: packed_cell_volume > 0.423077 -> 1.0
Bag: 6
Clustering: Weights C=[1], N=[1]
Test: pain=1 > 0.0 -> 0.058993198584726625
Test: capillary_refill_time=2 > 0.293233 -> 0.14489728957406045
Test: rectal_temperature > 0.407407 -> 0.299896391167891
Test: temperature_of_extremities=1 > 0.0 -> 0.4591479170272448
Test: nasogastric_tube=2 > 0.520619 -> 0.8112781244591328
Test: outcome=3 > 0.0 -> 0.18600577479746871
Test: respiratory_rate > 0.272727 -> 0.26899559358928116
Test: mucous_membranes=1 > 0.0 -> 1.0
Test: abdomen=1 > 0.0 -> 0.3814444125401065
Test: rectal_temperature > 0.407407 -> 0.2488423794274881
Test: abdominocentesis_appearance=2 > 0.0 -> 0.46956521111470706
Test: peripheral_pulse=1 > 0.0 -> 0.3219280948873623
Test: respiratory_rate > 0.25 -> 1.0
Bag: 7
Clustering: Weights C=[1], N=[1]
Test: abdominal_distension=2 > 0.264463 -> 0.11300151259119318
Test: rectal_temperature > 0.513461 -> 0.44477166784364586
Test: nasogastric_reflux=2 > 0.0 -> 0.9182958340544894
Test: abdominal_distension=4 > 0.157025 -> 0.0575293976188721
Test: mucous_membranes=3 > 0.223108 -> 0.12560786276335623
Test: peristalsis=3 > 0.5 -> 0.9852281360342515
Test: abdomcentesis_total_protein > 0.29396 -> 0.1382409511794473
Test: abdominal_distension=1 > 0.31405 -> 0.12686787062253235
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.2810361125534235
Test: packed_cell_volume > 0.230769 -> 0.5435644431995964
Bag: 8
Clustering: Weights C=[1], N=[1]
Test: mucous_membranes=3 > 0.223108 -> 0.05101676290141022
Test: surgery=2 > 0.0 -> 0.4464393446710156
Test: temperature_of_extremities=1 > 0.322314 -> 0.5435644431995964
Test: abdominal_distension=3 > 0.264463 -> 0.12471978968168895
Test: surgery=2 > 0.0 -> 0.3711624482834395
Test: total_protein > 0.056009 -> 0.3740571726123063
Test: pain=5 > 0.168724 -> 0.16070475288778252
Test: packed_cell_volume > 0.25 -> 0.21229006661701394
Test: nasogastric_tube=1 > 0.0 -> 0.3533593350214213
Bag: 9
Clustering: Weights C=[1], N=[1]
Test: packed_cell_volume > 0.288462 -> 0.07621660565340105
Test: pulse > 0.123377 -> 0.14194883401586977
Test: nasogastric_reflux_PH > 0.153846 -> 0.1543310994153817
Test: peripheral_pulse=2 > 0.0 -> 0.09127744624168022
Test: mucous_membranes=5 > 0.0 -> 0.31668908831502085
Test: respiratory_rate > 0.136364 -> 0.3219280948873623
Test: respiratory_rate > 0.318182 -> 1.0
Test: abdominal_distension=3 > 0.0 -> 0.16958442967043907
Test: abdominal_distension=4 > 0.0 -> 0.25642589168200325
Test: temperature_of_extremities=3 > 0.0 -> 0.31127812445913283
Test: mucous_membranes=2 > 0.0 -> 1.0
Test: peristalsis=2 > 0.0 -> 0.1908745046211094
Test: temperature_of_extremities=3 > 0.0 -> 0.2516291673878228
Test: respiratory_rate > 0.045455 -> 1.0
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.43949698692151334
Test: nasogastric_reflux=2 > 0.171875 -> 0.3912435636292556
Bag: 10
Clustering: Weights C=[1], N=[1]
Test: age=2 > 0.0 -> 0.05732528219095079
Test: abdominocentesis_appearance=1 > 0.0 -> 0.3059584928680419
Test: mucous_membranes=3 > 0.0 -> 0.6500224216483541
Test: total_protein > 0.031505 -> 0.4797830459416791
Test: rectal_temperature > 0.611111 -> 0.25104776383863525
Test: temperature_of_extremities=4 > 0.0 -> 0.8112781244591328
Test: respiratory_rate > 0.363636 -> 0.22853814395352803
Bag: 11
Clustering: Weights C=[1], N=[1]
Test: total_protein > 0.24684 -> 0.16610323941871963
Test: temperature_of_extremities=1 > 0.0 -> 0.14212677623445624
Test: peripheral_pulse=1 > 0.502183 -> 0.5184229837866563
Test: pain=2 > 0.0 -> 0.3059584928680419
Test: mucous_membranes=5 > 0.0 -> 0.3166890883150208
Test: abdominocentesis_appearance=2 > 0.0 -> 0.8112781244591328
Bag: 12
Clustering: Weights C=[1], N=[1]
Test: pain=5 > 0.0 -> 0.14753774408075193
Test: total_protein > 0.032672 -> 0.4537163391869449
Test: abdomen=4 > 0.234637 -> 0.06540648726322718
Test: peripheral_pulse=3 > 0.0 -> 0.3219280948873623
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.3671114697796741
Test: rectal_temperature > 0.703704 -> 0.2935644431995964
Test: surgical_lesion=2 > 0.0 -> 0.1601124942990118
Test: total_protein > 0.056009 -> 0.8631205685666311
Bag: 13
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=2 > 0.0 -> 0.06200309886085498
Test: mucous_membranes=2 > 0.0 -> 0.26856910287370145
Test: peripheral_pulse=2 > 0.0 -> 0.10917033867559878
Test: peristalsis=4 > 0.0 -> 0.9709505944546688
Test: peristalsis=1 > 0.149606 -> 0.10107777847191066
Test: surgery=2 > 0.0 -> 0.3219280948873623
Test: abdomen=2 > 0.0 -> 0.3788788371352293
Test: peristalsis=3 > 0.0 -> 0.2516291673878228
Test: abdomcentesis_total_protein > 0.19 -> 1.0
Bag: 14
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.314815 -> 0.22303463694269832
Test: respiratory_rate > 0.306818 -> 0.17212761077242222
Test: peristalsis=3 > 0.0 -> 0.5487949406953987
Test: age=2 > 0.0 -> 0.8112781244591328
Test: temperature_of_extremities=1 > 0.322314 -> 0.2277107801798582
Test: nasogastric_reflux=3 > 0.203125 -> 0.5216406363433185
Test: nasogastric_tube=1 > 0.365979 -> 0.31127812445913283
Test: peristalsis=3 > 0.0 -> 1.0
Bag: 15
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.30929931444577097
Test: nasogastric_tube=1 > 0.365979 -> 0.09054221403753882
Test: abdomen=4 > 0.0 -> 0.22943684069673975
Test: surgery=2 > 0.0 -> 0.8112781244591328
Test: abdominocentesis_appearance=2 > 0.0 -> 0.7219280948873623
Test: nasogastric_reflux=2 > 0.171875 -> 0.16904508196303292
Test: abdomen=2 > 0.0 -> 0.9182958340544894
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.22228483068568797
Bag: 16
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.19 -> 0.20551744927620463
Test: total_protein > 0.056009 -> 0.25846157931432867
Test: abdominocentesis_appearance=3 > 0.0 -> 0.3063064785355122
Test: abdominocentesis_appearance=2 > 0.0 -> 0.5185784610705099
Test: packed_cell_volume > 0.384615 -> 0.3166890883150208
Test: abdominal_distension=1 > 0.0 -> 1.0
Bag: 17
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=1 > 0.0 -> 0.14369321868327112
Test: surgery=2 > 0.0 -> 0.10226426007466938
Test: abdominal_distension=2 > 0.264463 -> 0.2576788051033315
Test: surgical_lesion=2 > 0.0 -> 0.46899559358928117
Test: total_protein > 0.049008 -> 0.6165533143863353
Test: mucous_membranes=5 > 0.099602 -> 0.6840384356390417
Bag: 18
Clustering: Weights C=[1], N=[1]
Test: total_protein > 0.038506 -> 0.5660653430868436
Test: mucous_membranes=3 > 0.0 -> 0.06372001807927907
Test: nasogastric_reflux_PH > 0.846154 -> 0.2471501836369669
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.1758558968939351
Test: nasogastric_reflux_PH > 0.461538 -> 0.2761954276479391
Bag: 19
Clustering: Weights C=[1], N=[1]
Test: nasogastric_reflux=2 > 0.171875 -> 0.07763548497576611
Test: packed_cell_volume > 0.288462 -> 0.1289364978558979
Test: pain=3 > 0.0 -> 0.1866787858450063
Test: temperature_of_extremities=2 > 0.0 -> 0.19845496251770922
Test: age=2 > 0.0 -> 0.31668908831502085
Test: nasogastric_reflux=3 > 0.0 -> 0.3219280948873623
Test: abdominal_distension=2 > 0.0 -> 1.0
Test: outcome=1 > 0.0 -> 0.16264044137387867
Test: temperature_of_extremities=3 > 0.0 -> 0.5916727785823274
Test: mucous_membranes=4 > 0.0 -> 0.3219280948873625
Test: total_protein > 0.056009 -> 0.4669171866886993
Test: abdominal_distension=2 > 0.0 -> 0.2516291673878228
Test: respiratory_rate > 0.090909 -> 1.0
Bag: 20
Clustering: Weights C=[1], N=[1]
Test: abdomen=4 > 0.234637 -> 0.15544809969332918
Test: total_protein > 0.24684 -> 0.5435644431995964
Test: abdominocentesis_appearance=3 > 0.0 -> 0.3005712034818069
Test: nasogastric_tube=2 > 0.0 -> 0.14161952533413782
Test: rectal_examination_feces > 0.0 -> 0.31127812445913283
Test: abdomen=3 > 0.0 -> 1.0
Test: rectal_temperature > 0.513461 -> 0.5297257989969674
Test: nasogastric_reflux=1 > 0.625 -> 0.19350684337293433
Test: mucous_membranes=1 > 0.0 -> 0.9182958340544894
Bag: 21
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.4218229043596755
Test: surgery=2 > 0.0 -> 0.13681603503398576
Test: temperature_of_extremities=1 > 0.322314 -> 0.4335941172605444
Test: mucous_membranes=3 > 0.0 -> 0.5032583347756457
Bag: 22
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.314815 -> 0.1054553435030231
Test: rectal_temperature > 0.796296 -> 0.08376570301157293
Test: peripheral_pulse=4 > 0.0 -> 0.09207845228807043
Test: nasogastric_tube=2 > 0.520619 -> 0.09335036636207966
Test: mucous_membranes=2 > 0.0 -> 0.3166890883150208
Test: packed_cell_volume > 0.423077 -> 1.0
Test: total_protein > 0.24684 -> 0.33554483001130886
Test: abdominal_distension=3 > 0.0 -> 0.1055646516052351
Test: pulse > 0.584416 -> 0.8112781244591328
Test: packed_cell_volume > 0.423077 -> 0.29245798554123414
Test: peristalsis=4 > 0.287402 -> 0.8112781244591328
Test: rectal_temperature > 0.5 -> 0.14269027946047558
Test: age=2 > 0.0 -> 0.8112781244591328
Bag: 23
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.29912806934383446
Test: total_protein > 0.03734 -> 0.22253762622154755
Test: surgery=2 > 0.0 -> 0.6099865470109876
Test: mucous_membranes=3 > 0.0 -> 0.7219280948873623
Bag: 24
Clustering: Weights C=[1], N=[1]
Test: total_protein > 0.24684 -> 0.33411198891679117
Test: surgery=2 > 0.0 -> 0.25855504877082314
Test: nasogastric_reflux_PH > 0.692308 -> 0.7219280948873623
Test: packed_cell_volume > 0.576923 -> 0.21396474138771832
Test: nasogastric_reflux=3 > 0.203125 -> 0.9709505944546688
Test: pain=2 > 0.0 -> 0.07912699124585176
Test: abdominal_distension=4 > 0.0 -> 0.5916727785823274
Bag: 25
Clustering: Weights C=[1], N=[1]
Test: outcome=3 > 0.0 -> 0.2502588683295588
Test: total_protein > 0.043174 -> 0.27276570047394344
Test: total_protein > 0.24684 -> 0.20443400292496483
Test: temperature_of_extremities=1 > 0.0 -> 0.3219280948873625
Test: nasogastric_reflux=1 > 0.0 -> 0.31127812445913283
Test: nasogastric_tube=2 > 0.0 -> 1.0
Test: abdomen=5 > 0.435754 -> 0.31345975017182415
Test: pulse > 0.363636 -> 1.0
Bag: 26
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.314815 -> 0.25751223256810785
Test: packed_cell_volume > 0.269231 -> 0.06892744039150289
Test: total_protein > 0.031505 -> 0.2081190468184917
Test: nasogastric_tube=3 > 0.0 -> 0.09572157485111565
Test: packed_cell_volume > 0.384615 -> 0.3604940656169301
Test: abdominocentesis_appearance=3 > 0.343284 -> 0.2335498363160155
Test: rectal_temperature > 0.513461 -> 0.9709505944546688
Bag: 27
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.3679981909020412
Test: abdominocentesis_appearance=2 > 0.0 -> 0.18915562711303913
Test: pain=2 > 0.242798 -> 0.1320966889958761
Test: temperature_of_extremities=3 > 0.0 -> 0.9182958340544894
Test: abdominal_distension=2 > 0.0 -> 0.1546249654505777
Test: peripheral_pulse=3 > 0.0 -> 0.44477166784364586
Test: abdomen=4 > 0.234637 -> 0.8631205685666311
Bag: 28
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=3 > 0.0 -> 0.3060516258076402
Test: nasogastric_tube=3 > 0.0 -> 0.060579227778377154
Test: abdominocentesis_appearance=1 > 0.0 -> 0.04041604892264228
Test: peripheral_pulse=3 > 0.441048 -> 0.1793291403859119
Test: pain=4 > 0.0 -> 0.9182958340544894
Test: mucous_membranes=1 > 0.0 -> 0.2935644431995964
Test: temperature_of_extremities=4 > 0.0 -> 0.5916727785823274
Test: abdomcentesis_total_protein > 0.19 -> 0.8366407419411673
Bag: 29
Clustering: Weights C=[1], N=[1]
Test: mucous_membranes=5 > 0.0 -> 0.12098628738944694
Test: abdominal_distension=2 > 0.264463 -> 0.40647928216725837
Test: temperature_of_extremities=1 > 0.0 -> 0.16428338622580352
Test: surgery=2 > 0.0 -> 0.19811742113040343
Test: abdominal_distension=2 > 0.0 -> 0.2516291673878228
Test: rectal_examination_feces > 0.333333 -> 1.0
Test: abdomen=5 > 0.0 -> 0.18905266854301622
Test: abdominocentesis_appearance=1 > 0.0 -> 0.9182958340544894
Bag: 30
Clustering: Weights C=[1], N=[1]
Test: abdomen=4 > 0.234637 -> 0.24429365856243956
Test: nasogastric_reflux=3 > 0.203125 -> 0.5435644431995964
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.11954843953444877
Test: respiratory_rate > 0.181818 -> 0.9709505944546688
Test: surgery=2 > 0.0 -> 0.04685931886954786
Test: peristalsis=1 > 0.0 -> 0.1382336456029663
Test: rectal_temperature > 0.481481 -> 1.0
Test: peripheral_pulse=3 > 0.0 -> 0.2863969571159562
Bag: 31
Clustering: Weights C=[1], N=[1]
Test: abdominal_distension=1 > 0.31405 -> 0.1144949981994472
Test: packed_cell_volume > 0.423077 -> 0.10902621798419032
Test: abdominocentesis_appearance=2 > 0.0 -> 0.27771966850258084
Test: pain=3 > 0.0 -> 0.2247875095893599
Test: temperature_of_extremities=3 > 0.446281 -> 0.2916919971380595
Test: peripheral_pulse=1 > 0.0 -> 0.9709505944546688
Test: age=2 > 0.0 -> 0.2537978408001329
Test: temperature_of_extremities=3 > 0.446281 -> 0.4763817583206179
Test: total_protein > 0.038506 -> 0.8631205685666311
Bag: 32
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=3 > 0.343284 -> 0.2396289603361943
Test: nasogastric_reflux=3 > 0.0 -> 0.4464393446710156
Test: peristalsis=3 > 0.0 -> 0.2935644431995964
Test: peripheral_pulse=3 > 0.0 -> 1.0
Test: peristalsis=3 > 0.0 -> 0.06430175868019422
Test: mucous_membranes=2 > 0.0 -> 0.19250568730904433
Test: rectal_temperature > 0.481481 -> 0.9182958340544894
Test: rectal_examination_feces > 0.0 -> 0.2974722489192897
Bag: 33
Clustering: Weights C=[1], N=[1]
Test: surgery=2 > 0.0 -> 0.0883891100632479
Test: pulse > 0.519481 -> 0.6665783579949205
Test: pain=5 > 0.168724 -> 0.31127812445913294
Test: capillary_refill_time=2 > 0.293233 -> 0.31668908831502085
Test: nasogastric_tube=1 > 0.365979 -> 0.2128966266607143
Test: rectal_temperature > 0.481481 -> 0.20443400292496505
Test: abdomen=2 > 0.0 -> 0.4591479170272448
Test: respiratory_rate > 0.255256 -> 0.31127812445913283
Test: mucous_membranes=3 > 0.0 -> 1.0
Bag: 34
Clustering: Weights C=[1], N=[1]
Test: total_protein > 0.24684 -> 0.30990328823553703
Test: abdominocentesis_appearance=2 > 0.0 -> 0.11683239335776008
Test: abdomen=3 > 0.0 -> 0.45901140621599945
Test: temperature_of_extremities=3 > 0.446281 -> 0.19163120400671674
Test: nasogastric_tube=1 > 0.0 -> 0.9182958340544894
Test: peripheral_pulse=4 > 0.0 -> 0.19811742113040332
Test: age=2 > 0.0 -> 0.9182958340544894
Test: abdominal_distension=3 > 0.0 -> 0.5055872616982602
Test: pain=4 > 0.0 -> 0.31127812445913283
Test: pulse > 0.194805 -> 1.0
Bag: 35
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.2088234122620295
Test: abdomcentesis_total_protein > 0.19 -> 0.07464271332703687
Test: pain=4 > 0.160494 -> 0.1045299801673939
Test: temperature_of_extremities=3 > 0.446281 -> 0.18290734961907595
Test: total_protein > 0.043174 -> 0.6165533143863353
Test: total_protein > 0.031505 -> 0.1831497988820474
Test: temperature_of_extremities=1 > 0.0 -> 0.9182958340544894
Bag: 36
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.41966295290732575
Test: pulse > 0.584416 -> 0.09978874394258336
Test: total_protein > 0.031505 -> 0.22118011551777125
Test: abdomen=5 > 0.0 -> 0.9182958340544894
Bag: 37
Clustering: Weights C=[1], N=[1]
Test: nasogastric_reflux=1 > 0.625 -> 0.12973024784151665
Test: pulse > 0.116883 -> 0.37950878258213616
Test: mucous_membranes=5 > 0.0 -> 0.38285033974200744
Test: surgical_lesion=2 > 0.0 -> 0.1793291403859119
Test: temperature_of_extremities=1 > 0.0 -> 0.9182958340544894
Test: abdominocentesis_appearance=1 > 0.0 -> 0.11400718987666902
Test: peripheral_pulse=1 > 0.502183 -> 0.22002600168808797
Test: nasogastric_tube=1 > 0.365979 -> 1.0
Test: abdomen=4 > 0.0 -> 0.3738926029228975
Test: temperature_of_extremities=3 > 0.446281 -> 0.5487949406953987
Test: respiratory_rate > 0.255256 -> 0.8112781244591328
Bag: 38
Clustering: Weights C=[1], N=[1]
Test: pain=4 > 0.0 -> 0.15562617819787516
Test: peripheral_pulse=1 > 0.0 -> 0.16486489706710927
Test: respiratory_rate > 0.363636 -> 0.2689955935892811
Test: rectal_examination_feces > 0.0 -> 0.08523484632956474
Test: total_protein > 0.038506 -> 0.2576788051033315
Test: rectal_examination_feces > 0.333333 -> 0.26899559358928116
Test: respiratory_rate > 0.045455 -> 1.0
Test: abdominocentesis_appearance=3 > 0.343284 -> 0.32365019815155605
Test: packed_cell_volume > 0.423077 -> 0.3788788371352293
Test: rectal_temperature > 0.5 -> 0.9182958340544894
Bag: 39
Clustering: Weights C=[1], N=[1]
Test: total_protein > 0.049008 -> 0.3149687702551589
Test: abdomen=2 > 0.0 -> 0.18424289179001144
Test: abdominal_distension=4 > 0.0 -> 0.4591479170272447
Test: pulse > 0.38961 -> 0.2516291673878228
Test: pulse > 0.273343 -> 1.0
Test: temperature_of_extremities=1 > 0.0 -> 0.3237384199045231
Test: abdominocentesis_appearance=3 > 0.343284 -> 0.6612262562697894
Test: pulse > 0.454545 -> 0.5435644431995964
Bag: 40
Clustering: Weights C=[1], N=[1]
Test: respiratory_rate > 0.272727 -> 0.17866956592083372
Test: total_protein > 0.056009 -> 0.5916727785823274
Test: abdominocentesis_appearance=3 > 0.343284 -> 0.39728503089379996
Test: abdomcentesis_total_protein > 0.29396 -> 0.320402072002678
Test: abdominocentesis_appearance=2 > 0.0 -> 0.1758558968939351
Test: peristalsis=2 > 0.0 -> 0.07897116812049665
Test: abdominocentesis_appearance=2 > 0.358209 -> 0.5916727785823274
Bag: 41
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.203704 -> 0.10605306382043711
Test: surgical_lesion=2 > 0.0 -> 0.06516655944082828
Test: respiratory_rate > 0.113636 -> 0.39827789673580605
Test: surgery=2 > 0.0 -> 0.9709505944546688
Test: abdominocentesis_appearance=2 > 0.0 -> 0.06105378373381021
Test: pulse > 0.584416 -> 0.3212122601251459
Test: capillary_refill_time=2 > 0.293233 -> 0.1920058354921066
Test: total_protein > 0.03734 -> 0.5487949406953987
Test: nasogastric_tube=2 > 0.520619 -> 0.8112781244591328
Test: abdomcentesis_total_protein > 0.19 -> 0.9182958340544894
Bag: 42
Clustering: Weights C=[1], N=[1]
Test: abdominal_distension=1 > 0.0 -> 0.07906013956823943
Test: abdominocentesis_appearance=2 > 0.0 -> 0.14865258200778278
Test: abdomen=5 > 0.435754 -> 0.9182958340544894
Test: capillary_refill_time=2 > 0.0 -> 0.29471705011630034
Test: peristalsis=4 > 0.0 -> 0.10549860554203849
Test: respiratory_rate > 0.306818 -> 0.7219280948873623
Test: abdominocentesis_appearance=3 > 0.0 -> 0.4669171866886993
Test: pain=1 > 0.0 -> 0.2516291673878228
Test: abdomen=5 > 0.0 -> 1.0
Bag: 43
Clustering: Weights C=[1], N=[1]
Test: pain=1 > 0.0 -> 0.05078474375023101
Test: age=2 > 0.0 -> 0.3502090290998973
Test: mucous_membranes=3 > 0.223108 -> 0.11168753696471317
Test: rectal_temperature > 0.513461 -> 0.7219280948873623
Test: total_protein > 0.24684 -> 0.40847260468307367
Test: abdominocentesis_appearance=2 > 0.0 -> 0.4591479170272447
Test: temperature_of_extremities=1 > 0.0 -> 0.9182958340544894
Bag: 44
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=2 > 0.0 -> 0.14105321878277355
Test: surgery=2 > 0.0 -> 0.17648803107924138
Test: abdominocentesis_appearance=2 > 0.358209 -> 0.24902249956730615
Test: temperature_of_extremities=4 > 0.0 -> 0.4591479170272447
Test: rectal_temperature > 0.740741 -> 0.9182958340544894
Test: temperature_of_extremities=1 > 0.322314 -> 0.5032583347756457
Test: peristalsis=3 > 0.0 -> 0.18310473570119623
Test: mucous_membranes=5 > 0.0 -> 0.6548575458269756
Test: mucous_membranes=3 > 0.0 -> 0.6500224216483541
Test: temperature_of_extremities=1 > 0.0 -> 0.19715972342414928
Test: respiratory_rate > 0.255256 -> 0.9182958340544894
Bag: 45
Clustering: Weights C=[1], N=[1]
Test: nasogastric_reflux=1 > 0.0 -> 0.19583545291248672
Test: abdomcentesis_total_protein > 0.19 -> 0.13593275681427686
Test: surgery=2 > 0.0 -> 0.40805852678064114
Test: pain=2 > 0.242798 -> 0.3095434291503252
Test: nasogastric_tube=1 > 0.0 -> 0.4204484631347319
Test: temperature_of_extremities=4 > 0.11157 -> 0.46899559358928117
Bag: 46
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=1 > 0.0 -> 0.13379258502740343
Test: packed_cell_volume > 0.423077 -> 0.21474491026057219
Test: total_protein > 0.056009 -> 0.49252708671426604
Test: pulse > 0.363636 -> 0.4335941172605444
Test: surgery=2 > 0.0 -> 0.5032583347756457
Test: packed_cell_volume > 0.423077 -> 0.2857457540097108
Test: total_protein > 0.056009 -> 1.0
Bag: 47
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=1 > 0.298507 -> 0.1219623771251861
Test: pulse > 0.220779 -> 0.5916727785823274
Test: abdominocentesis_appearance=3 > 0.0 -> 0.14092810678875722
Test: rectal_temperature > 0.314815 -> 0.3302824193321712
Test: pulse > 0.519481 -> 0.12936526187305242
Test: temperature_of_extremities=4 > 0.0 -> 0.9182958340544894
Test: abdominal_distension=2 > 0.0 -> 0.1025883968364304
Test: temperature_of_extremities=4 > 0.11157 -> 0.2576788051033315
Test: mucous_membranes=3 > 0.223108 -> 0.46899559358928117
Bag: 48
Clustering: Weights C=[1], N=[1]
Test: abdomcentesis_total_protein > 0.29396 -> 0.4974418725851553
Test: mucous_membranes=3 > 0.0 -> 0.1318395929163645
Test: packed_cell_volume > 0.423077 -> 0.12324791321663797
Test: surgery=2 > 0.0 -> 0.28103611255342353
Test: peristalsis=3 > 0.0 -> 1.0
Test: temperature_of_extremities=4 > 0.0 -> 1.0
Bag: 49
Clustering: Weights C=[1], N=[1]
Test: abdominocentesis_appearance=2 > 0.0 -> 0.12104091973889686
Test: rectal_examination_feces > 0.333333 -> 0.31495139791479687
Test: age=2 > 0.0 -> 0.18095733240984385
Test: pain=3 > 0.0 -> 0.37887883713522896
Test: abdominal_distension=1 > 0.31405 -> 0.9709505944546688
Test: respiratory_rate > 0.181818 -> 0.3655199733748161
Test: capillary_refill_time=2 > 0.0 -> 0.3219280948873625
Test: nasogastric_tube=1 > 0.0 -> 0.4669171866886993
Test: abdominal_distension=2 > 0.0 -> 0.9182958340544894
Bag: 50
Clustering: Weights C=[1], N=[1]
Test: rectal_temperature > 0.203704 -> 0.1350995402933417
Test: respiratory_rate > 0.306818 -> 0.16707745635687865
Test: abdomcentesis_total_protein > 0.19 -> 0.4204484631347319
Test: surgery=2 > 0.0 -> 0.26899559358928116
Test: peripheral_pulse=3 > 0.0 -> 1.0
Test: abdomcentesis_total_protein > 0.29396 -> 0.23910902851462257
Test: rectal_examination_feces > 0.0 -> 0.16814568846280115
Test: peripheral_pulse=2 > 0.0 -> 0.12166626108429474
Test: rectal_temperature > 0.513461 -> 0.8112781244591328
Test: peristalsis=2 > 0.0 -> 1.0
Induction Time: 0.492 sec
Pruning Time: 0.0 sec

Computing training error
Computing testing error
PredictionWriter is writing the ARFF header
Output written to: labeled.out
