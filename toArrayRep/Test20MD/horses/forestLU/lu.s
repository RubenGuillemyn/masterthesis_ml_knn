[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 7

[Attributes]
Descriptive=1-56
Disable=57
Target=1-56
Clustering=1-56
[Data]
File = lu.arff
TestSet = test.arff
