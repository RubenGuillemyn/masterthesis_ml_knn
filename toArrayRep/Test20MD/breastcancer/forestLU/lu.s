[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 3

[Attributes]
Descriptive=1-9
Disable=10
Target=1-9
Clustering=1-9
[Data]
File = lu.arff
TestSet = test.arff
