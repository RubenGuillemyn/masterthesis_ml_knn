[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 4

[Attributes]
Descriptive=1-20
Disable=21
Target=1-20
Clustering=1-20
[Data]
File = lu.arff
TestSet = test.arff
