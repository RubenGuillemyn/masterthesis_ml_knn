[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 6

[Attributes]
Descriptive=1-34
Disable=35
Target=1-34
Clustering=1-34
[Data]
File = lu.arff
TestSet = test.arff
