[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 11

[Attributes]
Descriptive=1-126
Disable=127
Target=1-126
Clustering=1-126
[Data]
File = lu.arff
TestSet = test.arff
