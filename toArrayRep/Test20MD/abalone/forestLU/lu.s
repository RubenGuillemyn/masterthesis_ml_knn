[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 3

[Attributes]
Descriptive=1-10
Disable=11
Target=1-10
Clustering=1-10
[Data]
File = lu.arff
TestSet = test.arff
