[Model]
MinimalWeight = 1.0

[Tree]
PruningMethod = None

[Constraints]
MaxDepth = 20

[Output]
WritePredictions = Test

[Ensemble]
EnsembleMethod = RForest
Iterations = 50
OOBestimate = No
Optimize = No
PrintPaths = Yes
PrintAllModels=Yes
SelectRandomSubspaces = 5

[Attributes]
Descriptive=1-26
Disable=27
Target=1-26
Clustering=1-26
[Data]
File = lu.arff
TestSet = test.arff
