package semisupervisedtrees.algorithms;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ruben
 */
public class SelfLearningTree {

    private ArrayList<Object[]> binaryAttrListLabeled = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListUnlabeled = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();
    private ArrayList<Object[]> attrListLabeled = new ArrayList<>();
    private ArrayList<Object[]> attrListUnlabeled = new ArrayList<>();
    private ArrayList<Object[]> attrListTest = new ArrayList<>();
    private List<Object> classification = new ArrayList<>();
    //Contains the header for creating a new trainingfile after SSL
    private List<String> header = new ArrayList<>();
    private int[][] closestN;

    private int kNN;
    private double threshHold;

    //Originele representatie
    public SelfLearningTree(ArrayList<Object[]> trainingOriginal, ArrayList<Object[]> unLabeled, ArrayList<Object[]> test, Set<Object> classification, int kNN, double threshHold) {
        this.kNN = kNN;
        this.threshHold = threshHold;
        this.classification.addAll(classification);
        this.attrListLabeled = trainingOriginal;
        this.attrListUnlabeled = unLabeled;
        this.attrListTest = test;
        this.header = header;
        selfLearningNoTrans(0);
        int[][] closest = findClosestNeighbors();
        HashMap<Integer, Object[]> model = evaluateNeighborsAndTest(closest, attrListTest);
        calculateEffect(model);
    }
    //Originele representatie SSL to create a new training file

    public SelfLearningTree(ArrayList<Object[]> trainingOriginal, ArrayList<Object[]> unLabeled, List<String> header, String fileName, Set<Object> classification, int kNN, double threshHold) {
        this.kNN = kNN;
        this.threshHold = threshHold;
        this.classification.addAll(classification);
        this.attrListLabeled = trainingOriginal;
        this.attrListUnlabeled = unLabeled;
        this.header = header;
        System.out.println("Starting with " + attrListLabeled.size() + " number of items");
        selfLearningNoTrans(0);
        System.out.println("Ending with " + attrListLabeled.size() + " number of items and remaining of "+attrListUnlabeled.size());
        //writeNewModelToFile(header, fileName);
    }

    private int[][] findClosestNeighbors() {
        closestN = new int[attrListTest.size()][kNN];
        for (int i = 0; i < attrListTest.size(); i++) {
            Map<Integer, Double> distances = new HashMap<>();
            for (int j = 0; j < attrListLabeled.size(); j++) {

                double manDist = calculateDist(attrListTest.get(i), attrListLabeled.get(j));
                distances.put(j, manDist);

            }
            //Sort list by distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < kNN; k++) {
                closestN[i][k] = it.next();
            }

        }
        return closestN;
    }

    private double calculateDist(Object[] fromEntry, Object[] otherEntry) {
        double dDistance = 0.0;
        for (int i = 0; i < fromEntry.length - 1; i++) {
            if (!fromEntry[i].equals(otherEntry[i])) {
                try {
                    double fromE = Double.parseDouble((String) fromEntry[i]);
                    double otherE = Double.parseDouble((String) otherEntry[i]);

                    dDistance += Math.abs(fromE - otherE);
                } catch (Exception exc) {
                    dDistance += 1;
                }
            }
        }
        return dDistance;
    }

    //This allows to sort on entries
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    private HashMap<Integer, Object[]> evaluateNeighborsAndTest(int[][] evaluateSet, ArrayList<Object[]> AttrList) {
        HashMap<Integer, Object[]> evaluate = new HashMap<>();
        for (int i = 0; i < AttrList.size(); i++) {
            Object counV = countVotes(i);
            Object[] classEntry = AttrList.get(i);
            Object classValue = classEntry[classEntry.length - 1];
            evaluate.put(i, new Object[]{classValue, counV});
        }
        return evaluate;
    }

    private Object countVotes(int index) {

        HashMap<Object, Integer> cVotes = new HashMap<>();
        for (Object myClassification : classification) {
            cVotes.put(myClassification, 0);
        }

        int[] neighBors = closestN[index];
        //algorithm for voting
        for (int i = 0; i < kNN; i++) {
            Object[] vector = attrListLabeled.get(neighBors[i]);
            // System.out.println(vector[vector.length-1]);
            if (vector == null || vector[vector.length - 1] == null || cVotes.get(vector[vector.length - 1]) == null) {
                System.out.println("Null or Target is not known of example");
            } else {
                cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);
            }

        }
        Map.Entry<Object, Integer> maxEntry = null;
        for (Map.Entry<Object, Integer> entry : cVotes.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        return maxEntry.getKey();
    }

    private void calculateEffect(HashMap<Integer, Object[]> model) {
        int nrOFCorrectPredictions = 0;
        for (Map.Entry<Integer, Object[]> entry : model.entrySet()) {
            if (entry.getValue()[0].equals(entry.getValue()[1])) {
                nrOFCorrectPredictions++;
            }
        }

        int percent = (nrOFCorrectPredictions * 100) / model.size();
        System.out.println("The kNN has an efficiency of:" + percent + " % or " + nrOFCorrectPredictions + " of " + attrListTest.size() + " nr. of examples");
    }

    private void selfLearningNoTrans(int step) {
        closestN = new int[attrListUnlabeled.size()][attrListLabeled.size()];

        for (int i = 0; i < attrListUnlabeled.size(); i++) {
            Map<Integer, Double> distances = new HashMap<>();
            for (int j = 0; j < attrListLabeled.size(); j++) {
                Double manDist = calculateDist(attrListUnlabeled.get(i), attrListLabeled.get(j));
                distances.put(j, manDist);

            }
            //Sort list by distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < attrListLabeled.size(); k++) {
                closestN[i][k] = it.next();
            }

        }
        boolean changed = false;
        for (int i = 0; i < attrListUnlabeled.size(); i++) {
            HashMap<Object, Integer> cVotes = new HashMap<>();
            for (Object myClassification : classification) {
                cVotes.put(myClassification, 0);
            }

            int[] neighBors = closestN[i];
            //algorithm for voting

            for (int j = 0; j < kNN; j++) {
                Object[] vector = attrListLabeled.get(neighBors[j]);
                // System.out.println(vector[vector.length-1]);
                //Classification is not known by training
                if (vector == null || vector[vector.length - 1] == null || cVotes.get(vector[vector.length - 1]) == null) {
                    //System.out.println("Null");
                } else {
                    cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);
                }

            }

            for (Object entry : cVotes.keySet()) {
                double percent = (cVotes.get(entry) * 100) / kNN;
                if (percent >= threshHold) {
                    try {
                        attrListUnlabeled.get(i)[attrListUnlabeled.get(i).length - 1] = entry;

                        attrListLabeled.add(attrListUnlabeled.get(i));
                        attrListUnlabeled.remove(attrListUnlabeled.get(i));
                        changed = true;
                    } catch (Exception exc) {

                    }
                }
            }
        }
        if (changed) {        
            step += 1;
            //selfLearningNoTrans(step);
        } else {
            System.out.println("Nr of steps: " + step);
        }

    }

    private void selfLearningTrans(int step) {
        closestN = new int[binaryAttrListUnlabeled.size()][binaryAttrListLabeled.size()];
        for (int i = 0; i < binaryAttrListUnlabeled.size(); i++) {
            Map<Integer, Double> distances = new HashMap<>();
            for (int j = 0; j < binaryAttrListLabeled.size(); j++) {
                Double manDist = calculateDist(binaryAttrListUnlabeled.get(i), binaryAttrListLabeled.get(j));
                distances.put(j, manDist);

            }
            //Sort list by distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < binaryAttrListLabeled.size(); k++) {
                closestN[i][k] = it.next();
            }

        }
        boolean changed = false;
        for (int i = 0; i < binaryAttrListUnlabeled.size(); i++) {
            HashMap<Object, Integer> cVotes = new HashMap<>();
            for (Object myClassification : classification) {
                cVotes.put(myClassification, 0);
            }

            int[] neighBors = closestN[i];
            //algorithm for voting
            int top = (neighBors.length / 4);
            for (int j = 0; j < top; j++) {
                Object[] vector = binaryAttrListLabeled.get(neighBors[j]);
                // System.out.println(vector[vector.length-1]);
                cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);

            }

            for (Object entry : cVotes.keySet()) {
                double percent = (cVotes.get(entry) * 100) / 20;
                if (percent > threshHold) {
                    try {
                        binaryAttrListLabeled.add(binaryAttrListUnlabeled.get(i));
                        binaryAttrListUnlabeled.remove(binaryAttrListUnlabeled.get(i));
                        changed = true;
                    } catch (Exception exc) {

                    }
                }
            }
        }
        if (changed) {
            step += 1;
            selfLearningNoTrans(step);
        } else {
            System.out.println("Nr of steps: " + step);
        }

    }

    private void writeNewModelToFile(List<String> header, String fileName) {
        PrintWriter modelWriter = null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/toArrayRep/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();

            //This allows for crossvalidation
            Collections.shuffle(attrListLabeled);
            // String fileName = header.get(0).substring(8, header.get(0).length()-1);
            modelWriter = new PrintWriter(path + "/labeled2.arff");

            for (String lineOfHeader : header) {
                modelWriter.write(lineOfHeader + "\n");
            }

            for (int i = 0; i < attrListLabeled.size(); i++) {
                for (int j = 0; j < attrListLabeled.get(i).length; j++) {
                    if (j != attrListLabeled.get(i).length - 1) {
                        modelWriter.write(attrListLabeled.get(i)[j].toString() + ",");
                    } else {
                        modelWriter.write(attrListLabeled.get(i)[j].toString() + "\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                modelWriter.close();
//                svmWriterT.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

}
