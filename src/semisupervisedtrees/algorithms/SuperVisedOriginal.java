package semisupervisedtrees.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ruben
 */
public class SuperVisedOriginal {

    private ArrayList<Object[]> attrList = new ArrayList<>();
    private ArrayList<Object[]> attrListTest = new ArrayList<>();
    private Set<Object> classification = new HashSet<>();
    private int[][] closestN;
    private HashMap<Object, HashMap<Integer, HashMap<Object, Integer>>> averages;

    private int kNN;

    public SuperVisedOriginal(ArrayList<Object[]> training, ArrayList<Object[]> test, Set<Object> classification, int kNN) {
        this.kNN = kNN;
        this.attrList = training;
        this.attrListTest = test;
        this.classification = classification;
        findClosestNeighbors();
        HashMap<Integer, Object[]> model = evaluateNeighborsAndTest();
        calculateEffect(model);
    }

    public SuperVisedOriginal(ArrayList<Object[]> training, ArrayList<Object[]> test, Set<Object> classification, HashMap<Object, HashMap<Integer, HashMap<Object, Integer>>> averagesNominal, int kNN) {
        this.kNN = kNN;
        this.attrList = training;
        this.attrListTest = test;
        this.classification = classification;
        this.averages = averagesNominal;
        findClosestNeighbors();
        HashMap<Integer, Object[]> model = evaluateNeighborsAndTest();
        calculateEffect(model);
    }

    private int[][] findClosestNeighbors() {
        closestN = new int[attrListTest.size()][kNN];
        for (int i = 0; i < attrListTest.size(); i++) {
            Map<Integer, Double> distances = new HashMap<>();
            for (int j = 0; j < attrList.size(); j++) {
                //calculates manhattan distance
                double manH = calculateDist(attrListTest.get(i), attrList.get(j));
                distances.put(j, manH);

            }
            //Sort list by distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < kNN; k++) {
                closestN[i][k] = it.next();
            }

        }
        return closestN;
    }

    private double calculateDist(Object[] fromEntry, Object[] otherEntry) {

        double dDistance = 0.0;
        for (int i = 0; i < fromEntry.length - 1; i++) {
            if (!fromEntry[i].equals(otherEntry[i])) {
                try {
                    double fromE = Double.parseDouble((String) fromEntry[i]);
                    double otherE = Double.parseDouble((String) otherEntry[i]);

                    dDistance += Math.abs(fromE - otherE);
                } catch (Exception exc) {
                    dDistance += 1;
                }
            }
        }
        return dDistance;
    }
    //This allows to sort on entries

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    private HashMap<Integer, Object[]> evaluateNeighborsAndTest() {
        HashMap<Integer, Object[]> evaluate = new HashMap<>();
        for (int i = 0; i < attrListTest.size(); i++) {
            Object counV = countVotes(i);
            Object[] classEntry = attrListTest.get(i);
            Object classValue = classEntry[classEntry.length - 1];
            evaluate.put(i, new Object[]{classValue, counV});
        }
        return evaluate;
    }

    // Make objected orientated => hardcoded for the moment on different classifications
    private Object countVotes(int index) {

        HashMap<Object, Integer> cVotes = new HashMap<>();
        for (Object myClassification : classification) {
            cVotes.put(myClassification, 0);
        }

        int[] neighBors = closestN[index];
        //algorithm for voting
        for (int i = 0; i < kNN; i++) {
            Object[] vector = attrList.get(neighBors[i]);
            // System.out.println(vector[vector.length-1]);
            cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);

        }
        Map.Entry<Object, Integer> maxEntry = null;
        for (Map.Entry<Object, Integer> entry : cVotes.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        return maxEntry.getKey();
    }

    private void calculateEffect(HashMap<Integer, Object[]> model) {
        int nrOFCorrectPredictions = 0;
        for (Map.Entry<Integer, Object[]> entry : model.entrySet()) {
            if (entry.getValue()[0].equals(entry.getValue()[1])) {
                nrOFCorrectPredictions++;
            }
        }
        int percent = (nrOFCorrectPredictions * 100) / model.size();
        System.out.println("The kNN with supervised learning & original representation has an efficiency of:" + percent + " % or " + nrOFCorrectPredictions + " on " + attrListTest.size() + " nr. of examples");
    }



}
