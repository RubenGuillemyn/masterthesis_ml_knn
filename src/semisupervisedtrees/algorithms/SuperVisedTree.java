package semisupervisedtrees.algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ruben
 */
public class SuperVisedTree {

    private ArrayList<Object[]> binaryAttrList = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();
    private Set<Object> classification = new HashSet<>();
    private int[][] closestN;

    private int kNN;

    public SuperVisedTree(String fileNameLabeled, String fileNameTestL, int kNN) {
        this.kNN = kNN;
        readRFBIFiles(fileNameLabeled, fileNameTestL);
        findClosestNeighbors();
        HashMap<Integer, Object[]> model = evaluateNeighborsAndTest();
        calculateEffect(model);
    }

    public List<Object[]> readRFBIFiles(String fileNameLabeled, String fileNameTestL) {
    BufferedReader br = null;
        FileReader fr = null;
      
        BufferedReader br3 = null;
        FileReader fr3 = null;
 
        int totNrLAttr = 0;
        int totNrLUAttr = 0;
        try {

            File labeled = new File("toArrayRep/" + fileNameLabeled);
            System.out.println(labeled.getAbsolutePath());
            fr = new FileReader(labeled.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
            while (!sCurrentLine.toLowerCase().contains("class")) {
                try {
                    totNrLAttr = Integer.parseInt(sCurrentLine.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sCurrentLine = br.readLine();
            }
            System.out.println("Total Number of labeled attributes: " + totNrLAttr);
         

            File tLabeled = new File("toArrayRep/" + fileNameTestL);
            fr3 = new FileReader(tLabeled.getAbsolutePath());
            br3 = new BufferedReader(fr3);

      
           
            System.out.println("Total Number of Unlabeled attributes: " + totNrLUAttr);

            String tempLine = br3.readLine();
            while (!tempLine.toLowerCase().contains("class")) {
                tempLine = br3.readLine();
            }
           
            String sCurrentLT = "";
            //Skips empty lines
            for (int i = 0; i < 3; i++) {
                sCurrentLine = br.readLine();     
                sCurrentLT = br3.readLine();
            }

            while (sCurrentLine != null ) {

                //Removes the "{}"
                sCurrentLine = sCurrentLine.substring(1, sCurrentLine.length() - 1);
              

                Object[] exampleEntries = new Object[totNrLAttr + totNrLUAttr + 1];

                List<String> itemsL = new ArrayList<>(Arrays.asList(sCurrentLine.split(", ")));
          
                for (int i = 0; i < itemsL.size() ; i++) {
                    int index = Integer.parseInt(itemsL.get(i).split(" ")[0]);
                    Object value = itemsL.get(i).split(" ")[1];
                    exampleEntries[index-1] = value;
                    //Stores the unique values
                    if (i == itemsL.size() - 1) {
                        classification.add(value);
                    }
                }
               
                binaryAttrList.add(exampleEntries);
                sCurrentLine = br.readLine();
             

            }
            //TESTFILE 
            while (sCurrentLT != null ) {

                sCurrentLT = sCurrentLT.substring(1, sCurrentLT.length() - 1);
              
                Object[] exampleTest = new Object[totNrLAttr + totNrLUAttr + 1];
                List<String> itemsLT = new ArrayList<>(Arrays.asList(sCurrentLT.split(", ")));
            
                for (int i = 0; i < itemsLT.size() ; i++) {
                    int index = Integer.parseInt(itemsLT.get(i).split(" ")[0]);
                    Object value = itemsLT.get(i).split(" ")[1];
                    exampleTest[index-1] = value;

                }
                binaryAttrListTest.add(exampleTest);
                sCurrentLT = br3.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();

                br3.close();
                fr3.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }

        return binaryAttrList;
    }


    private int[][] findClosestNeighbors() {
        closestN = new int[binaryAttrListTest.size()][kNN];
        for (int i = 0; i < binaryAttrListTest.size(); i++) {
            Map<Integer, Integer> distances = new HashMap<>();
            for (int j = 0; j < binaryAttrList.size(); j++) {
                //calculates manhattan distance
                int manH = calculateDist(binaryAttrListTest.get(i), binaryAttrList.get(j));
                distances.put(j, manH);

            }
            //Sort list by distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < kNN; k++) {
                closestN[i][k] = it.next();
            }

        }
        return closestN;
    }

    private int calculateDist(Object[] fromEntry, Object[] otherEntry) {
        int distance = 0;
        for (int i = 0; i < fromEntry.length-1; i++) {
            if (fromEntry[i]!=otherEntry[i]) {
                distance++;
            }
        }
        return distance;
    }

    //This allows to sort on entries
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    private HashMap<Integer, Object[]> evaluateNeighborsAndTest() {
        HashMap<Integer, Object[]> evaluate = new HashMap<>();
        for (int i = 0; i < binaryAttrListTest.size(); i++) {
            Object counV = countVotes(i);
            Object[] classEntry = binaryAttrListTest.get(i);
            Object classValue = classEntry[classEntry.length - 1];
            evaluate.put(i, new Object[]{classValue, counV});
        }
        return evaluate;
    }

    // Make objected orientated => hardcoded for the moment on different classifications
    private Object countVotes(int index) {

        HashMap<Object, Integer> cVotes = new HashMap<>();
        for (Object myClassification : classification) {
            cVotes.put(myClassification, 0);
        }
        int ones = 0;
        int twos = 0;
        int[] neighBors = closestN[index];
        //algorithm for voting
        for (int i = 0; i < kNN; i++) {
            Object[] vector = binaryAttrList.get(neighBors[i]);
            // System.out.println(vector[vector.length-1]);
            cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);

        }
        Map.Entry<Object, Integer> maxEntry = null;
        for (Map.Entry<Object, Integer> entry : cVotes.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        return maxEntry.getKey();
    }

    private void calculateEffect(HashMap<Integer, Object[]> model) {
        int nrOFCorrectPredictions = 0;
        for (Map.Entry<Integer, Object[]> entry : model.entrySet()) {
            if (entry.getValue()[0].equals(entry.getValue()[1])) {
                nrOFCorrectPredictions++;
            }
        }

        int percent = (nrOFCorrectPredictions * 100) / model.size();
        System.out.println("The kNN with supervised learning has an efficiency of:" + percent + " % on " + binaryAttrListTest.size() + " nr. of examples");
    }

}
