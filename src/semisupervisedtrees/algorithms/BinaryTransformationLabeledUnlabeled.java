package semisupervisedtrees.algorithms;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


public class BinaryTransformationLabeledUnlabeled {

    private ArrayList<Object[]> binaryAttrList = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();
    private Set<Object> classification = new HashSet<>();
    private int[][] closestN;

    private int kNN;

    public BinaryTransformationLabeledUnlabeled(ArrayList<Object[]> training,ArrayList<Object[]> test, Set<Object> classification, int kNN) {
        this.kNN = kNN;
        this.binaryAttrList = training;
        this.binaryAttrListTest = test;
        this.classification=classification;
        findClosestNeighbors();
        HashMap<Integer, Object[]> model = evaluateNeighborsAndTest();
        calculateEffect(model);
    }

  

    private int[][] findClosestNeighbors() {
        closestN = new int[binaryAttrListTest.size()][kNN];
        for (int i = 0; i < binaryAttrListTest.size(); i++) {
            Map<Integer, Integer> distances = new HashMap<>();
            for (int j = 0; j < binaryAttrList.size(); j++) {

                int manDist = calculateDist(binaryAttrListTest.get(i), binaryAttrList.get(j));
                distances.put(j, manDist);

            }
            //Sort list by incremented distance
            distances = sortByValue(distances);
            Set<Integer> entries = distances.keySet();
            Iterator<Integer> it = entries.iterator();
            for (int k = 0; k < kNN; k++) {
                closestN[i][k] = it.next();
            }

        }
        return closestN;
    }

    private int calculateDist(Object[] fromEntry, Object[] otherEntry) {
        int distance = 0;
        for (int i = 0; i < fromEntry.length-1; i++) {
            if (!fromEntry[i].equals(otherEntry[i])) {
                distance++;
            }
        }
        return distance;
    }

    //This allows to sort on entries
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    private HashMap<Integer, Object[]> evaluateNeighborsAndTest() {
        HashMap<Integer, Object[]> evaluate = new HashMap<>();
        for (int i = 0; i < binaryAttrListTest.size(); i++) {
            Object counV = countVotes(i);
            Object[] classEntry = binaryAttrListTest.get(i);
            Object classValue = classEntry[classEntry.length - 1];
            evaluate.put(i, new Object[]{classValue, counV});
        }
        return evaluate;
    }

    // Make objected orientated => hardcoded for the moment on different classifications
    private Object countVotes(int index) {

        HashMap<Object, Integer> cVotes = new HashMap<>();
        for (Object myClassification : classification) {
            cVotes.put(myClassification, 0);
        }
        int[] neighBors = closestN[index];
        //algorithm for  majority voting
        for (int i = 0; i < kNN; i++) {
            Object[] vector = binaryAttrList.get(neighBors[i]);
            cVotes.put(vector[vector.length - 1], cVotes.get(vector[vector.length - 1]) + 1);

        }
        Map.Entry<Object, Integer> maxEntry = null;
        for (Map.Entry<Object, Integer> entry : cVotes.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        return maxEntry.getKey();
    }
    /*
    * Calculates the percentage of the correct predictions
    */
    private void calculateEffect(HashMap<Integer, Object[]> model) {
        int nrOFCorrectPredictions = 0;
        for (Map.Entry<Integer, Object[]> entry : model.entrySet()) {
            if (entry.getValue()[0] .equals(entry.getValue()[1])) {
                nrOFCorrectPredictions++;
            }
        }
        int percent = (nrOFCorrectPredictions * 100) / model.size();
        System.out.println("The kNN has an efficiency of:" + percent + " % binary or " + nrOFCorrectPredictions+ " of "+ binaryAttrListTest.size() + " nr. of examples");
    }

}
