/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semisupervisedtrees.randomizeFiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Ruben
 */
public class ClusRandomizeFiles {

    private List<String> header = new ArrayList<>();
    private List<String> entriesMap = new ArrayList<>();

    public ClusRandomizeFiles(String inputFile) {
        
            randomizeFiles(inputFile);
       
        
    }

    public void randomizeFiles(String arffFile) {
        BufferedReader br = null;
        FileReader fr = null;
        PrintWriter writerL = null;
        PrintWriter writerLU = null;
        PrintWriter writerT = null;

        try {
            File file = new File(arffFile);
            String dirName = file.getName().split("\\.")[0];
            File mkDir = new File(System.getProperty("user.dir")+"/DataRandom/"+dirName);
            System.out.println("Output file is:"+ mkDir);
            Boolean mkDirSucc = mkDir.mkdirs();
            
            System.out.println(file.getAbsolutePath());
            fr = new FileReader(file.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = "";
            while (!sCurrentLine.toLowerCase().contains("@data")) {
                sCurrentLine = br.readLine();
                header.add(sCurrentLine);

            }
            while (sCurrentLine != null) {
                sCurrentLine = br.readLine();
                if(sCurrentLine!=null){
                     entriesMap.add(sCurrentLine);
                }
               
            }
            //Randomizes the entries
            Collections.shuffle(entriesMap);
            String path = mkDir.getAbsolutePath();
            writerL = new PrintWriter(path+"/labeled.arff");
            writerLU = new PrintWriter(path+"/lu.arff");
            writerT = new PrintWriter(path+"/test.arff");
            for (int i = 0; i < header.size(); i++) {
                String line = header.get(i) + "\n";
                writerL.write(line.toString());
                writerLU.write(line.toString());
                writerT.write(line.toString());
            }
            int nrOfTest = (int) (entriesMap.size() * 0.2);
            int nrOfLabeled = (int) ((entriesMap.size() - nrOfTest) * 0.20);
            //Checks if training are lesser then 20 otherwise take 50
            if(nrOfLabeled < 20){
                nrOfLabeled = 20;
            }
            
            int nrOfUnlabeled = (int) (entriesMap.size() - nrOfTest);
            for (int i = 0; i < entriesMap.size(); i++) {
                String line = entriesMap.get(i) + "\n";
                if (!line.equals("")) {
                    if (i < nrOfUnlabeled) {
                        if (i < nrOfLabeled) {
                             writerL.write(line.toString());
                        }
                        //Line for Methode Dzeroski
                        //line  = line.substring(0, line.length()-2)+"?\n";
                        writerLU.write(line.toString());
                       
                    } else {
                       
                        writerT.write(line.toString());
                    }
                     //System.out.println(line.toString());
                }

            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                writerL.close();
                writerLU.close();
                writerT.close();
                br.close();
                fr.close();
            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }

    }

}
