/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import selflearningSVM.SelfLearningThreshold;
import semisupervisedtrees.algorithms.BinaryTransformationLabeledUnlabeled;
import semisupervisedtrees.algorithms.BinaryTransformationSelfLearning;
import semisupervisedtrees.algorithms.BinaryTransformationUnsupervised;
import semisupervisedtrees.algorithms.SelfLearningTree;
import semisupervisedtrees.algorithms.SuperVisedOriginal;
import semisupervisedtrees.randomizeFiles.ClusRandomizeFiles;

/**
 *
 * @author Ruben
 */
public class StartUp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//         ClusRandomizeFiles random = new ClusRandomizeFiles("ICUAWMRC.arff");

//        ReadIn readIn = new ReadIn("labeled.arff");
//        ReadInUnLabeled readInUn = new ReadInUnLabeled("lu.arff");
//        ReadInTest readT = new ReadInTest("test.arff");
//        
        //When LU attributes are too high memory overflow
               ReadInTransformedPureSupervised readTransSV = new ReadInTransformedPureSupervised("labeled.transformed.arff");
               ReadInTransformedTestSupervised readTransSVT = new ReadInTransformedTestSupervised("test.transformed.arff");

        //When LU attributes are not generating memory overflow
//        ReadInTransformed readTrans = new ReadInTransformed("labeled.transformed.arff", "lu.transformed.arff");
//        ReadInTransformedTest readTestTr = new ReadInTransformedTest("test.transformed.arff", "testlu.transformed.arff");

        //ReadInTransformedOther readTransOther = new ReadInTransformedOther("labeled.transformed.arff", "lu.transformed.arff");
//        WriteOutSVM svmWriter = new WriteOutSVM("", readTrans.getTrainingBoth(), readTestTr.getTest(), readTrans.getClassification());
//        WriteOutSVM svmWriterOriginal = new WriteOutSVM("", readIn.getTraining(), readInUn.getUnlabeledData(),readIn.getClassification());
//          WriteOutSVM svmWriterSSLSVM = new WriteOutSVM("", readTransSV.getTainingL(), readTransSVT.getTestLabeledOnly(),readTransSV.getClassification());
//        WriteOutSVMThreshhold svmThreshWriter = new WriteOutSVMThreshhold("", readIn.getTraining(),readInUn.getUnlabeledData(), readT.getTest(), readIn.getClassification());
//           WriteOutSVMThreshhold svmThreshWriterTrans = new WriteOutSVMThreshhold("", readTrans.getTainingL(),readTrans.getTrainingUnlabeled(), readT.getTest(), readTrans.getClassification());

        //When  LU attributes are not too big
//        BinaryTransformationLabeledUnlabeled test2 = new BinaryTransformationLabeledUnlabeled(readTrans.getTainingL(), readTestTr.getTestLabeledOnly(), readIn.getClassification(), 5);
//        BinaryTransformationLabeledUnlabeled test = new BinaryTransformationLabeledUnlabeled(readTrans.getTrainingBoth(), readTestTr.getTest(), readIn.getClassification(), 5);
//        SuperVisedOriginal test3 = new SuperVisedOriginal(readIn.getTraining(), readT.getTest(), readIn.getClassification(), null, 5);
          //SELF-LEARNING
//        SelfLearningTree slTree = new SelfLearningTree(readIn.getTraining(), readInUn.getUnlabeledData(), readT.getTest(), readIn.getClassification(), 5, 80);
          

        //When LU are too big
        BinaryTransformationLabeledUnlabeled test2 = new BinaryTransformationLabeledUnlabeled(readTransSV.getTainingL(), readTransSVT.getTestLabeledOnly(), readTransSV.getClassification(), 5);       
//        SuperVisedOriginal test3 = new SuperVisedOriginal(readIn.getTraining(), readT.getTest(), readIn.getClassification(), 5);
//        SelfLearningTree slTree = new SelfLearningTree(readIn.getTraining(), readInUn.getUnlabeledData(),readIn.getHeader(),"",readIn.getClassification(),5,80);


//        SelfLearningThreshold slt = new SelfLearningThreshold("fPredict.predict", 0.80, readIn.getTraining(), readInUn.getUnlabeledData(),readIn.getClassification());
//        WriteOutSVM svmWriter = new WriteOutSVM("", slt.getAdjustedSet(), readT.getTest(), readIn.getClassification());
//           SelfLearningThreshold slt = new SelfLearningThreshold("fPredict.predict", 0.80, readIn.getHeader(),"labeled", readIn.getTraining(), readInUn.getUnlabeledData(),readIn.getClassification());

      
    }

}
