/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author Ruben
 */
class WriteOutSVMThreshhold {

    private ArrayList<Object[]> binaryLabeled = new ArrayList<>();
    private ArrayList<Object[]> binaryUnlabeled = new ArrayList<>();
    private ArrayList<Object[]> binaryTest = new ArrayList<>();
    private ArrayList<Object> classification = new ArrayList<>();

    public WriteOutSVMThreshhold(String fileName, ArrayList<Object[]> binaryLabeled,ArrayList<Object[]> binaryUnlabeled, ArrayList<Object[]> binaryTest, Set<Object> classification) {
        this.binaryLabeled = binaryLabeled;
        this.binaryUnlabeled= binaryUnlabeled;
        this.binaryTest = binaryTest;
        this.classification.addAll(classification);
        writeSVMPrepped(fileName);
        writeSVMTest(fileName);
    }

    private void writeSVMPrepped(String fileName) {

      
        PrintWriter svmWriter = null;
        PrintWriter svmWriterU=null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/DataSVMTH/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();

            //This allows for crossvalidation
            Collections.shuffle(binaryLabeled);
            svmWriter = new PrintWriter(path + "/svmPrepped" + fileName + ".txt");
            svmWriterU = new PrintWriter(path + "/testLU" + fileName + ".txt");


            for (int i = 0; i < binaryLabeled.size(); i++) {
                
                    //Prints class as first element in row
                    int index = classification.indexOf(binaryLabeled.get(i)[binaryLabeled.get(i).length - 1].toString());
                    svmWriter.write(index + "\t");
                    for (int j = 0; j < binaryLabeled.get(i).length - 1; j++) {
                        int indexInFile = j + 1;
                        svmWriter.write(indexInFile + ":" + binaryLabeled.get(i)[j] + "\t");
                    }
                    svmWriter.write("\n");
               
                  
            }
            
            for (int i = 0; i < binaryUnlabeled.size(); i++) {           
                    //Prints class as first element in row
                    int index = classification.indexOf(binaryUnlabeled.get(i)[binaryUnlabeled.get(i).length - 1].toString());
                    svmWriterU.write(index + "\t");
                    for (int j = 0; j < binaryUnlabeled.get(i).length - 1; j++) {
                        int indexInFile = j + 1;
                        svmWriterU.write(indexInFile + ":" + binaryUnlabeled.get(i)[j] + "\t");
                    }
                    svmWriterU.write("\n");
               
                  
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                svmWriter.close();
                svmWriterU.close();
               
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

    private void writeSVMTest(String fileName) {
        PrintWriter svmWriterTestComplete = null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/DataSVMTH/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();
            //This allows for crossvalidation
            Collections.shuffle(binaryLabeled);
            svmWriterTestComplete = new PrintWriter(path + "/finalTest" + fileName + ".txt");

            for (int i = 0; i < binaryTest.size(); i++) {
                int index = classification.indexOf(binaryTest.get(i)[binaryTest.get(i).length - 1].toString());
                //Prints class as first element in row
                svmWriterTestComplete.write(index + "\t");
                for (int j = 0; j < binaryTest.get(i).length - 1; j++) {
                    int indexInFile = j + 1;
                    svmWriterTestComplete.write(indexInFile + ":" + binaryTest.get(i)[j] + "\t");
                }
                svmWriterTestComplete.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                svmWriterTestComplete.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

}
