/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class ReadInTransformedTest {
    
    
 
    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListUnlabeled= new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListTestL = new ArrayList<>();
    private Set<Object> classification = new HashSet<>();

    public ArrayList<Object[]> getTest() {
        return binaryAttrListTest;
    }
    
     public ArrayList<Object[]> getTestLabeledOnly() {
        return binaryAttrListTestL;
    }
     public ArrayList<Object[]> getUnlabeledOnly(){
         return binaryAttrListUnlabeled;
     }



    public ReadInTransformedTest(String fileNameTestL, String fileNameTestLU) {
        
        readRFBIFiles(fileNameTestL, fileNameTestLU);
      
     
    }

    public List<Object[]> readRFBIFiles( String fileNameTestL, String fileNameTestLU) {

        BufferedReader br3 = null;
        FileReader fr3 = null;
        BufferedReader br4 = null;
        FileReader fr4 = null;
        int totNrTest = 0;
        int totNrTestUn=0;
        try {

           
            File tLabeled = new File("toArrayRep/" + fileNameTestL);
            fr3 = new FileReader(tLabeled.getAbsolutePath());
            br3 = new BufferedReader(fr3);

            File tuLabeled = new File("toArrayRep/" + fileNameTestLU);
            fr4 = new FileReader(tuLabeled.getAbsolutePath());
            br4 = new BufferedReader(fr4);

            String sTest = br3.readLine();
            while (!sTest.toLowerCase().contains("class")) {
                try {
                    totNrTest = Integer.parseInt(sTest.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sTest = br3.readLine();
            }
           // System.out.println("Total Number of Labeled attributes: " + totNrTest);
            //String sTestUn="";
               String sTestUn = br4.readLine();
            while (!sTestUn.toLowerCase().contains("class")) {
                try {
                    totNrTestUn = Integer.parseInt(sTestUn.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sTestUn = br4.readLine();
                
                
            }
            //System.out.println("Total Number of UnLabeled attributes: " + totNrTestUn);
   
            //Skips empty lines
            for (int i = 0; i < 3; i++) {
                sTest = br3.readLine();
                sTestUn = br4.readLine();
            }

            while (sTest != null && sTestUn != null) {

                //Removes the "{}"
                sTest = sTest.substring(1, sTest.length() - 1);
                sTestUn = sTestUn.substring(1, sTestUn.length() - 1);

                Object[] exampleEntries = new Object[totNrTest+totNrTestUn +1];
                Object[] exampleEntriesL = new Object[totNrTest+1];
                Arrays.fill(exampleEntries, "0");
                Arrays.fill(exampleEntriesL, "0");
                List<String> itemsL = new ArrayList<>(Arrays.asList(sTest.split(", ")));
                List<String> itemsLU = new ArrayList<>(Arrays.asList(sTestUn.split(", ")));
                for (int i = 0; i < itemsL.size() ; i++) {
                    int index = Integer.parseInt(itemsL.get(i).split(" ")[0]);
                    Object value = itemsL.get(i).split(" ")[1];
                    exampleEntriesL[index-1] = value;
                    
                    if(i < itemsL.size()-2){
                        exampleEntries[index - 1] = value;
                        
                    }
                    //Stores the unique values                  
                    if (i == itemsL.size() - 1) {
                        classification.add(value);
                        exampleEntries[exampleEntries.length-1]=value;
                    }
                }
                binaryAttrListTestL.add(exampleEntriesL);
                for (int i = 0; i < itemsLU.size()-1; i++) {
                    int index = Integer.parseInt(itemsLU.get(i).split(" ")[0]);
                    Object value = itemsLU.get(i).split(" ")[1];
                    exampleEntries[(index + totNrTest) - 1] = value;
                    //Stores the unique values
                    if (i == itemsLU.size() - 1) {
                        classification.add(value);
                    }
                }
              
                binaryAttrListTest.add(exampleEntries);

                sTest = br3.readLine();
                sTestUn = br4.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {

                br3.close();
                fr3.close();
                fr4.close();
                br4.close();
            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
      
        System.out.println("The nr of examples in testdata: "+binaryAttrListTest.size());

        return binaryAttrListTest;
    }
}
