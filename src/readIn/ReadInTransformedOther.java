/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
class ReadInTransformedOther {

    private ArrayList<Object[]> binaryAttrListBoth = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrList = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListLu = new ArrayList<>();

    private Set<Object> classification = new HashSet<>();

    private int kNN;

    public ArrayList<Object[]> getTrainingBoth() {
        return binaryAttrListBoth;
    }

    public ArrayList<Object[]> getTainingL() {
        return binaryAttrList;
    }

    public ArrayList<Object[]> getTrainingLU() {
        return binaryAttrListLu;
    }

    public Set<Object> getClassification() {
        return classification;
    }

    public ReadInTransformedOther(String fileNameLabeled, String fileNameUnlabeled) {
        this.kNN = kNN;
        readRFBIFiles(fileNameLabeled, fileNameUnlabeled);

    }

    public List<Object[]> readRFBIFiles(String fileNameLabeled, String fileNameUnlabeled) {

        BufferedReader br = null;
        FileReader fr = null;
        BufferedReader br2 = null;
        FileReader fr2 = null;

        int totNrLAttr = 0;
        int totNrLUAttr = 0;
        try {

            File labeled = new File("toArrayRep/" + fileNameLabeled);
            System.out.println(labeled.getAbsolutePath());
            fr = new FileReader(labeled.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
            while (!sCurrentLine.toLowerCase().contains("class")) {
                try {
                    totNrLAttr = Integer.parseInt(sCurrentLine.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sCurrentLine = br.readLine();
            }
            System.out.println("Total Number of labeled attributes: " + totNrLAttr);
            File unLabeled = new File("toArrayRep/" + fileNameUnlabeled);
            fr2 = new FileReader(unLabeled.getAbsolutePath());
            br2 = new BufferedReader(fr2);
//
            //String sCurrentLineUn="";
            String sCurrentLineUn = br2.readLine();
            while (!sCurrentLineUn.toLowerCase().contains("class")) {
                try {
                    totNrLUAttr = Integer.parseInt(sCurrentLineUn.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sCurrentLineUn = br2.readLine();
            }
            System.out.println("Total Number of Unlabeled attributes: " + totNrLUAttr);
//
//            //Skips empty lines
            for (int i = 0; i < 3; i++) {
                sCurrentLine = br.readLine();
                sCurrentLineUn = br2.readLine();

            }

            while (sCurrentLine != null && sCurrentLineUn != null) {

                //Removes the "{}"
                sCurrentLine = sCurrentLine.substring(1, sCurrentLine.length() - 1);
                sCurrentLineUn = sCurrentLineUn.substring(1, sCurrentLineUn.length() - 1);
                //Total number of attributes + class
                Object[] exampleEntries = new Object[totNrLAttr + totNrLUAttr + 1];
                Object[] exampleEntriesL = new Object[totNrLAttr + 1];
                Object[] exampleEntriesLu = new Object[totNrLUAttr];
                Arrays.fill(exampleEntries, "0");
                Arrays.fill(exampleEntriesL, "0");
                Arrays.fill(exampleEntriesLu, "0");

                List<String> itemsL = new ArrayList<>(Arrays.asList(sCurrentLine.split(", ")));
                List<String> itemsLU = new ArrayList<>(Arrays.asList(sCurrentLineUn.split(", ")));
                for (int i = 0; i < itemsL.size(); i++) {
                    //Stores the unique values
                    int index = Integer.parseInt(itemsL.get(i).split(" ")[0]);
                    Object value = itemsL.get(i).split(" ")[1];

                    if (i == itemsL.size() - 1) {
                        classification.add(value);
                        exampleEntries[exampleEntries.length-1]=value;
                        exampleEntriesL[exampleEntriesL.length-1]=value;
                    } else {
                        exampleEntriesL[index - 1] = value;
                        exampleEntries[index - 1] = value;
                    }

                }
                binaryAttrList.add(exampleEntriesL);
                for (int i = 0; i < itemsLU.size()-1; i++) {
                    int index = Integer.parseInt(itemsLU.get(i).split(" ")[0]);
                    Object value = itemsLU.get(i).split(" ")[1];
                    exampleEntries[(index + totNrLAttr) - 1] = value;
                    exampleEntriesLu[index - 1] = value;
                }
                binaryAttrListLu.add(exampleEntriesLu);
                binaryAttrListBoth.add(exampleEntries);
                sCurrentLine = br.readLine();
                sCurrentLineUn = br2.readLine();

            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();
                br2.close();
                fr2.close();
            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
        System.out.println("The nr of attributes of trainingdata: " + binaryAttrListBoth.size());
        return binaryAttrListBoth;
    }
}
