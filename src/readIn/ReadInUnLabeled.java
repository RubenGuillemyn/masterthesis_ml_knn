/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class ReadInUnLabeled {

    private ArrayList<Object[]> binaryAttrListUnlabeled = new ArrayList<>();

    private HashMap<Integer, HashMap<Object, Integer>> averages = new HashMap<>();

    public ArrayList<Object[]> getUnlabeledData() {
        return binaryAttrListUnlabeled;
    }

    public Set<Object> getClassification() {
        return classification;
    }
    private Set<Object> classification = new HashSet<>();

    public ReadInUnLabeled(String fileNameUnLabeled) {
        readFile(fileNameUnLabeled);
    }

    public List<Object[]> readFile(String fileNameLabeled) {

        BufferedReader br = null;
        FileReader fr = null;

        int totNrLAttr = 0;

        try {
            File labeled = new File("toArrayRep/" + fileNameLabeled);
            System.out.println(labeled.getAbsolutePath());
            fr = new FileReader(labeled.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
            while (!sCurrentLine.toLowerCase().contains("@attribute class")) {
                totNrLAttr++;
                sCurrentLine = br.readLine();
            }
            System.out.println("Total Number of unlabeled attributes: " + totNrLAttr);
            boolean dataRule = true;
            //Skips empty lines
            while (dataRule) {
                sCurrentLine = br.readLine();

                if (sCurrentLine.toLowerCase().contains("data")) {
                    sCurrentLine = br.readLine();
                    dataRule = false;
                }
            }
            while (sCurrentLine != null) {
                HashMap<Object, Integer> averageItem;
                Object[] exampleEntries = new Object[totNrLAttr -1];

                List<String> itemsL = new ArrayList<>(Arrays.asList(sCurrentLine.split(",")));

                for (int i = 0; i < itemsL.size(); i++) {
                    averageItem = averages.get(new Integer(i));
                    if (averageItem == null) {
                        averageItem = new HashMap<>();
                    }
                 
                        if (averageItem.containsKey(itemsL.get(i))) {
                            averageItem.put(itemsL.get(i), averageItem.get(itemsL.get(i)) + 1);
                        } else {
                            averageItem.put(itemsL.get(i), 1);
                        }
                        averages.put(new Integer(i), averageItem);

                        exampleEntries[i] = itemsL.get(i);
                        //Stores the unique values
                        if (i == itemsL.size() - 1) {
                            classification.add(itemsL.get(i));
                            binaryAttrListUnlabeled.add(exampleEntries);
                        }
                    

                }
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
        System.out.println("Nr of Unlabeled examples: " + binaryAttrListUnlabeled.size());
        return binaryAttrListUnlabeled;
    }
}
