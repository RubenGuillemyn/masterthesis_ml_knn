/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class ReadIn {

    private ArrayList<Object[]> binaryAttrListLabeled = new ArrayList<>();
    //Contains the header of the file so self-learning can be done
    private List<String> header = new ArrayList<>();

    public ArrayList<Object[]> getTraining() {
        return binaryAttrListLabeled;
    }

    public List<String> getHeader() {
        return header;
    }

    public Set<Object> getClassification() {
        return classification;
    }

    private Set<Object> classification = new HashSet<>();

    public ReadIn(String fileNameLabeled) {
        readFile(fileNameLabeled);
    }

    public List<Object[]> readFile(String fileNameLabeled) {
        BufferedReader br = null;
        FileReader fr = null;
        int totNrLAttr = 0;
        try {
            File labeled = new File("toArrayRep/" + fileNameLabeled);
            System.out.println(labeled.getAbsolutePath());
            fr = new FileReader(labeled.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
             header.add(sCurrentLine);
            while (!sCurrentLine.toLowerCase().contains("@attribute class")) {
                totNrLAttr++;
               
                sCurrentLine = br.readLine();
                 header.add(sCurrentLine);
            }
            System.out.println("Total Number of labeled attributes: " + (totNrLAttr - 1));

            boolean dataRule = true;
            //Skips empty lines
            while (dataRule) {
                sCurrentLine = br.readLine();
                header.add(sCurrentLine);
                if (sCurrentLine.toLowerCase().contains("data")) {
                    sCurrentLine = br.readLine();
                    dataRule = false;
                }
            }
            //averages = new HashMap<>();
            while (sCurrentLine != null) {
                Object[] exampleEntries = new Object[totNrLAttr - 1];
                List<String> itemsL = new ArrayList<>(Arrays.asList(sCurrentLine.split(",")));

                for (int i = 0; i < itemsL.size(); i++) {
                    //Calculate numeric values

                    //if (!sCurrentLine.contains("?")) {
                        exampleEntries[i] = itemsL.get(i);
                        //Stores the unique values
                        if (i == itemsL.size() - 1) {
                            classification.add(itemsL.get(i));
                            binaryAttrListLabeled.add(exampleEntries);
                        }
                    //}

                }
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
        System.out.println("Nr of training: " + binaryAttrListLabeled.size());

        //Determining the averages
        return binaryAttrListLabeled;
    }
}
