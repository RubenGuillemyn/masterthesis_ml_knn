/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Ruben
 */
public class ReadInTest {

    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();

    public ReadInTest(String testFile) {
            readTestFiles(testFile);
    }

    public ArrayList<Object[]> getTest() {
        return binaryAttrListTest;
    }

    public List<Object[]> readTestFiles(String fileNameTestL) {

        BufferedReader br3 = null;
        FileReader fr3 = null;

        int totNrAttr = 0;

        try {
            File tLabeled = new File("toArrayRep/" + fileNameTestL);
            fr3 = new FileReader(tLabeled.getAbsolutePath());
            br3 = new BufferedReader(fr3);

            String sTestLine = br3.readLine();
            while (!sTestLine.toLowerCase().contains("@attribute class")) {
                totNrAttr++;

                sTestLine = br3.readLine();
            }
            //System.out.println("Total Number of attributes: " + totNrAttr);

            boolean dataRule = true;
           
            while(!sTestLine.toLowerCase().contains("data")){
                sTestLine = br3.readLine();
            }

            sTestLine=br3.readLine();

              
            //TESTFILE 
            while (sTestLine!=null) {

                sTestLine = sTestLine.substring(0, sTestLine.length());

                Object[] exampleTest = new Object[totNrAttr-1];
                List<String> itemsLT = new ArrayList<>(Arrays.asList(sTestLine.split(",")));

                for (int i = 0; i < itemsLT.size(); i++) {
                    exampleTest[i] = itemsLT.get(i);
                }

                binaryAttrListTest.add(exampleTest);

                sTestLine = br3.readLine();

            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {

                br3.close();
                fr3.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
        System.out.println("Number of test: "+binaryAttrListTest.size());
        return binaryAttrListTest;

    }
}
