/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class ReadInTransformedPureSupervised {
    
    private ArrayList<Object[]> binaryAttrList = new ArrayList<>();


    private Set<Object> classification = new HashSet<>();

    private int kNN;

   

    public ArrayList<Object[]> getTainingL() {
        return binaryAttrList;
    }


    

    public Set<Object> getClassification() {
        return classification;
    }

    public ReadInTransformedPureSupervised(String fileNameLabeled) {
        this.kNN = kNN;
        readRFBIFiles(fileNameLabeled);

    }

    public List<Object[]> readRFBIFiles(String fileNameLabeled) {
       
        BufferedReader br = null;
        FileReader fr = null;

        int totNrLAttr = 0;
        try {

            File labeled = new File("toArrayRep/" + fileNameLabeled);
            System.out.println(labeled.getAbsolutePath());
            fr = new FileReader(labeled.getAbsolutePath());
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
            while (!sCurrentLine.toLowerCase().contains("class")) {
                try {
                    totNrLAttr = Integer.parseInt(sCurrentLine.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sCurrentLine = br.readLine();
            }
            System.out.println("Total Number of labeled attributes: " + totNrLAttr);
           
//
//            //Skips empty lines
            for (int i = 0; i < 3; i++) {
                sCurrentLine = br.readLine();
               

            }

            while (sCurrentLine != null ) {

                //Removes the "{}"
                sCurrentLine = sCurrentLine.substring(1, sCurrentLine.length() - 1);
                

    
                Object[] exampleEntriesL = new Object[totNrLAttr+1];
                Arrays.fill(exampleEntriesL, "0");


                List<String> itemsL = new ArrayList<>(Arrays.asList(sCurrentLine.split(", ")));
                
                for (int i = 0; i < itemsL.size(); i++) {
                    int index = Integer.parseInt(itemsL.get(i).split(" ")[0]);
                    Object value = itemsL.get(i).split(" ")[1];
                    exampleEntriesL[index-1]= value;
                   
                    //Stores the unique values
                    if (i == itemsL.size() - 1) {
                        classification.add(value);
                    }
                }
                binaryAttrList.add(exampleEntriesL);

                sCurrentLine = br.readLine();


            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();
            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
        return binaryAttrList;
    }
}
