/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class ReadInTransformedTestSupervised {

    private ArrayList<Object[]> binaryAttrListTestL = new ArrayList<>();
    private Set<Object> classification = new HashSet<>();

     
     public ArrayList<Object[]> getTestLabeledOnly() {
        return binaryAttrListTestL;
    }



    public ReadInTransformedTestSupervised(String fileNameTestL) {
        readRFBIFiles(fileNameTestL);
    }

    public List<Object[]> readRFBIFiles( String fileNameTestL) {

        BufferedReader br3 = null;
        FileReader fr3 = null;
        int totNrTest = 0;
        try {

            File tLabeled = new File("toArrayRep/" + fileNameTestL);
            fr3 = new FileReader(tLabeled.getAbsolutePath());
            br3 = new BufferedReader(fr3);

            String sTest = br3.readLine();
            while (!sTest.toLowerCase().contains("@attribute class")) {
                try {
                    totNrTest = Integer.parseInt(sTest.replaceAll("[\\D]", ""));

                } catch (NumberFormatException nExc) {

                }
                sTest = br3.readLine();
            }
           // System.out.println("Total Number of Labeled attributes: " + totNrTest);
   
            //Skips empty lines
            for (int i = 0; i < 3; i++) {
                sTest = br3.readLine();
            }
            
            while (sTest != null &&binaryAttrListTestL.size()< 2500) {

                //Removes the "{}"
                sTest = sTest.substring(1, sTest.length() - 1);
                
                Object[] exampleEntriesL = new Object[totNrTest+1];
                Arrays.fill(exampleEntriesL, "0");
                List<String> itemsL = new ArrayList<>(Arrays.asList(sTest.split(", ")));
                for (int i = 0; i < itemsL.size() ; i++) {
                    int index = Integer.parseInt(itemsL.get(i).split(" ")[0]);
                    Object value = itemsL.get(i).split(" ")[1];
                    exampleEntriesL[index-1]=value;
                    //Stores the unique values
                    if (i == itemsL.size() - 1) {
                        classification.add(value);
                    }
                }
                binaryAttrListTestL.add(exampleEntriesL);
                
                sTest = br3.readLine();
                
                   
                
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {

                br3.close();
                fr3.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }
      
        System.out.println("The nr of examples in testdata: "+binaryAttrListTestL.size());

        return binaryAttrListTestL;
    }
}
