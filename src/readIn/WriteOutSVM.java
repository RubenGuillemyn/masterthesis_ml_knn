/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readIn;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class WriteOutSVM {

    private ArrayList<Object[]> binaryAttrListBoth = new ArrayList<>();
    private ArrayList<Object[]> binaryAttrListTest = new ArrayList<>();
    private ArrayList<Object> classification = new ArrayList<>();

    public WriteOutSVM(String fileName, ArrayList<Object[]> binaryAttrListBoth, ArrayList<Object[]> binaryAttrListTest, Set<Object> classification) {
        this.binaryAttrListBoth = binaryAttrListBoth;
        this.binaryAttrListTest = binaryAttrListTest;
        this.classification.addAll(classification);
        writeSVMPrepped(fileName);
        writeSVMTest(fileName);
    }

    private void writeSVMPrepped(String fileName) {

        PrintWriter svmWriterT = null;
        PrintWriter svmWriter = null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/DataSVM/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();

            //This allows for crossvalidation
            Collections.shuffle(binaryAttrListBoth);
            svmWriter = new PrintWriter(path + "/svmPrepped" + fileName + ".txt");
       
            for (int i = 0; i < binaryAttrListBoth.size(); i++) {
               
                    //Prints class as first element in row
                    int index = classification.indexOf(binaryAttrListBoth.get(i)[binaryAttrListBoth.get(i).length - 1].toString());
                    if(index == -1){
                        index = Integer.parseInt(binaryAttrListBoth.get(i)[binaryAttrListBoth.get(i).length - 1].toString());
                    }
                    svmWriter.write(index + "\t");
                    for (int j = 0; j < binaryAttrListBoth.get(i).length - 1; j++) {
                        int indexInFile = j + 1;
                        svmWriter.write(indexInFile + ":" + binaryAttrListBoth.get(i)[j] + "\t");
                    }
                    svmWriter.write("\n");
               
                   
                
                //System.out.println(line.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                svmWriter.close();
//                svmWriterT.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

    private void writeSVMTest(String fileName) {
        PrintWriter svmWriterTestComplete = null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/DataSVM/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();
            //This allows for crossvalidation
            Collections.shuffle(binaryAttrListBoth);
            svmWriterTestComplete = new PrintWriter(path + "/finalTest" + fileName + ".txt");

            for (int i = 0; i < binaryAttrListTest.size(); i++) {
                int index = classification.indexOf(binaryAttrListTest.get(i)[binaryAttrListTest.get(i).length - 1].toString());
                //Prints class as first element in row
                svmWriterTestComplete.write(index + "\t");
                for (int j = 0; j < binaryAttrListTest.get(i).length - 1; j++) {
                    int indexInFile = j + 1;
                    svmWriterTestComplete.write(indexInFile + ":" + binaryAttrListTest.get(i)[j] + "\t");
                }
                svmWriterTestComplete.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                svmWriterTestComplete.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }
}
