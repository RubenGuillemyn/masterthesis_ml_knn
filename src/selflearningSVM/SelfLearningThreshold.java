/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package selflearningSVM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Ruben
 */
public class SelfLearningThreshold {

    private ArrayList<Object[]> labeledData;
    private ArrayList<Object[]> unlabeledData;
    private List<Object> classification = new ArrayList<>();
    private double threshold;
    private List<String> header=new ArrayList<>();

    public ArrayList<Object[]> getAdjustedSet() {
        return labeledData;
    }

    public SelfLearningThreshold(String unLabeledSVM, double threshold, ArrayList<Object[]> labeled, ArrayList<Object[]> unlabeled, Set<Object> classification) {
        this.threshold = threshold;
        this.labeledData = labeled;
        this.unlabeledData = unlabeled;
        this.classification.addAll(classification);
        createThresholdedFile(unLabeledSVM);     
    }
      public SelfLearningThreshold(String unLabeledSVM, double threshold,List<String> header,String fileName, ArrayList<Object[]> labeled, ArrayList<Object[]> unlabeled, Set<Object> classification) {
        this.threshold = threshold;
        this.labeledData = labeled;
        this.unlabeledData = unlabeled;
        this.header=header;
        this.classification.addAll(classification);
        createThresholdedFile(unLabeledSVM);    
        writeNewModelToFile(header, fileName);
    }

    private void createThresholdedFile(String unLabeledSVM) {
        System.out.println("There are " + labeledData.size() + " number of entries before threshold");
        BufferedReader br = null;
        FileReader fr = null;
        try {
            File unLabeled = new File("toArrayRep/" + unLabeledSVM);

            fr = new FileReader(unLabeled.getAbsolutePath());
            br = new BufferedReader(fr);
            //Reads in the header of the file
            String sCurrentLine = br.readLine();

            sCurrentLine = br.readLine();
            List<String> items = new ArrayList<>(Arrays.asList(sCurrentLine.split(" ")));
            double percentageC = 0;
            double percentageD= 0;
            if (items.size() > 3) {
                System.out.println("Warning check number of variables to be sure of number of variables in target");
                percentageC = Double.parseDouble(items.get(3));
                //percentageD = Double.parseDouble(items.get(4));
            }
            int indexOfFile = 0;
            while (sCurrentLine != null && indexOfFile < unlabeledData.size()) {
                items = new ArrayList<>(Arrays.asList(sCurrentLine.split(" ")));
                double percentageA = Double.parseDouble(items.get(1));
                double percentageB = Double.parseDouble(items.get(2));

                if (percentageA > threshold || percentageB > threshold || percentageC > threshold || percentageD > threshold) {
                    try {
                        int indexInClassification = Integer.parseInt(items.get(0));
                        unlabeledData.get(indexOfFile)[unlabeledData.get(indexOfFile).length - 1] = classification.get(indexInClassification);
                        labeledData.add(unlabeledData.get(indexOfFile));
                    }
                    catch(Exception e){
                        
                    }

                }
                indexOfFile++;
                sCurrentLine = br.readLine();
            }
            System.out.println("There are " + labeledData.size() + " number of entries AFTER threshold ");

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                br.close();
                fr.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }

        }

    }
     private void writeNewModelToFile(List<String> header, String fileName) {
        PrintWriter modelWriter = null;

        try {
            File mkDir = new File(System.getProperty("user.dir") + "/SLSVM/");
            Boolean mkDirSucc = mkDir.mkdirs();
            String path = mkDir.getAbsolutePath();

            //This allows for crossvalidation
            Collections.shuffle(labeledData);
            // String fileName = header.get(0).substring(8, header.get(0).length()-1);
            modelWriter = new PrintWriter(path + "/" + fileName + ".arff");

            for (String lineOfHeader : header) {
                modelWriter.write(lineOfHeader + "\n");
            }

            for (int i = 0; i < labeledData.size(); i++) {
                for (int j = 0; j < labeledData.get(i).length; j++) {
                    if (j != labeledData.get(i).length - 1) {
                        modelWriter.write(labeledData.get(i)[j].toString() + ",");
                    } else {
                        modelWriter.write(labeledData.get(i)[j].toString() + "\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                modelWriter.close();
//                svmWriterT.close();

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

}
